import os
import random

import numpy as np

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.optimizers import Adam
from datetime import datetime

import matplotlib.pyplot as plt

import gym

class env_surrogate(gym.Env):


    def __init__(self,env,s0):
        self.current_action = None
        self.initial_conditions = []
        self.__version__ = "0.0.1"

        self.MAX_TIME = 50
        self.is_finalized = False
        self.current_episode = -1

        self.action_episode_memory = []
        self.rewards = []
        self.current_steps = 0
        self.TOTAL_COUNTER = 0

        self.seed()

        self.action_space = env.action_space

        self.observation_space = env.observation_space

        self.threshold = env.threshold

        self.env_real = env

        self.s0 = s0
        self.model = None





    def step(self, action):

        state, reward = self._take_action(action)


        self.action_episode_memory[self.current_episode].append(action)


        self.current_steps += 1
        if self.current_steps > self.MAX_TIME:
            self.is_finalized = True


        self.rewards[self.current_episode].append(reward)

        if (reward > self.threshold):
            self.is_finalized = True

        return state, reward, self.is_finalized, {}


    def _take_action(self, action, is_optimisation = False):

        state = self.model.predict([[self.s.squeeze()],[action]])
        self.s = state

        reward = self.reward_model.predict([state])
        reward = reward.squeeze()
        return state, reward


    def set_model(self,model):
        self.model = model

    def set_reward_model(self,reward_model):
        self.reward_model = reward_model


    def reset(self, **kwargs):

        self.is_finalized = False
        self.current_episode += 1
        self.current_steps = 0
        self.action_episode_memory.append([])
        self.rewards.append([])

        #action = self.action_space.sample()
        action = np.random.uniform(-1., 1., self.action_space.shape[-1])

        self.s = self.model.predict([[self.s0],[action]])

        return_initial_state = np.squeeze(self.s)
        self.initial_conditions.append([return_initial_state])

        return self.s #return_initial_state

    def seed(self, seed=None):
        random.seed(seed)

    def accumulate_data(self,n_data,s1_list=None,s2_list=None,a_list=None,r_list = None,dtype=tf.float64):

        if(s1_list == None):
            s1_list = []
        if(s2_list == None):
            s2_list = []
        if(a_list == None):
            a_list = []
        if(r_list == None):
            r_list = []

        #fully random
        # for _ in range(n_data):
        #     s1 = self.env_real.reset()
        #     a = self.env_real.action_space.sample()
        #     s2,r,done,_ = self.env_real.step(a)
        #     s1_list.append(s1)
        #     a_list.append(a)
        #     s2_list.append(s2)
        #     r_list.append(r)

        #random in trajectories
        counter = 0
        while(counter<n_data):
            s1 = self.env_real.reset()
            a = self.env_real.action_space.sample()
            s2, r, done, _ = self.env_real.step(a)
            s1_list.append(s1)
            a_list.append(a)
            s2_list.append(s2)
            r_list.append(r)

            counter+=1
            while(not done):
                s1 = s2
                a = self.env_real.action_space.sample()
                s2,r,done,_ = self.env_real.step(a)
                s1_list.append(s1)
                a_list.append(a)
                s2_list.append(s2)
                r_list.append(r)

                counter+=1

        return s1_list,a_list,s2_list,r_list

    def _define_model(self,hidden_sizes, dtype=tf.float64):
        obs_shape = self.env_real.observation_space.shape
        act_shape = self.env_real.action_space.shape
        (obs_dim,) = obs_shape
        input_x = tf.keras.Input(shape=obs_shape, dtype=dtype)
        input_u = tf.keras.Input(shape=act_shape, dtype=dtype)
        hidden = tf.keras.layers.concatenate([input_x, input_u], dtype=dtype)
        for size in hidden_sizes:
            hidden = tf.keras.layers.Dense(
                size,
                activation='relu',
                dtype=dtype,
                kernel_regularizer=tf.keras.regularizers.l1(1e-6),
                bias_regularizer=tf.keras.regularizers.l1(1e-6),
            )(hidden)
        output = tf.keras.layers.Dense(
            obs_dim,
            activation='linear',
            dtype=dtype,
            kernel_regularizer=tf.keras.regularizers.l1(1e-6),
            bias_regularizer=tf.keras.regularizers.l1(1e-6),
        )(hidden)
        model = tf.keras.Model(inputs=[input_x, input_u], outputs=output)
        model.compile(optimizer=Adam(0.001), loss='mse')
        print(model.summary())
        return model

    def _define_reward_mapping_model(self,hidden_sizes,dtype=tf.float64):
        obs_shape = self.env_real.observation_space.shape
        input_x = tf.keras.Input(shape=obs_shape, dtype=dtype)

        for i in range(len(hidden_sizes)):
            if(i>0):
                hidden = tf.keras.layers.Dense(
                    hidden_sizes[i],
                    activation='relu',
                    dtype=dtype,
                    kernel_regularizer=tf.keras.regularizers.l1(1e-6),
                    bias_regularizer=tf.keras.regularizers.l1(1e-6),
                )(hidden)
            else:
                hidden = tf.keras.layers.Dense(
                    hidden_sizes[i],
                    activation='relu',
                    dtype=dtype,
                    kernel_regularizer=tf.keras.regularizers.l1(1e-6),
                    bias_regularizer=tf.keras.regularizers.l1(1e-6),
                )(input_x)
        output = tf.keras.layers.Dense(
            1,
            activation='linear',
            dtype=dtype,
            kernel_regularizer=tf.keras.regularizers.l1(1e-6),
            bias_regularizer=tf.keras.regularizers.l1(1e-6),
        )(hidden)
        model = tf.keras.Model(inputs=input_x, outputs=output)
        model.compile(optimizer=Adam(0.01), loss='mse')
        print(model.summary())
        return model

    def train_model(self, s1_list, a_list, s2_list, dtype=tf.float32):
        data_model = []
        for i in range(len(s1_list)):
            data_model.append((s1_list[i], a_list[i], s2_list[i]))
        random.shuffle(data_model)
        lists = zip(*data_model)
        s1_tens, a_tens, s2_tens = [tf.convert_to_tensor(x, dtype=dtype) for x in lists]

        timestamp = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

        model = self._define_model([10, 10])

        history = model.fit(
            x=(s1_tens, a_tens),
            y=s2_tens,
            validation_split=0.2,
            verbose=1,
            epochs=200,
            callbacks=[
                # tf.keras.callbacks.EarlyStopping(
                #     monitor='val_loss',
                #     mode='min',
                #     patience=5,
                # ),
                tf.keras.callbacks.ModelCheckpoint(
                    filepath=os.path.join('models', f'AWAKE_{timestamp}_model.ckpt'),
                    save_weights_only=True,
                    verbose=1,
                ),
            ],
        )

        fig,ax = plt.subplots()
        plt.plot(history.history['loss'], label='loss')
        plt.plot(history.history['val_loss'], label='val_loss')
        plt.xlabel('# epoch')
        plt.ylabel('error')
        plt.legend()
        plt.pause(1)

        model.save(os.path.join('models', f'AWAKE_{timestamp}_model.h5'))

        return model



    def train_reward_mapping(self,s2_list,r_list,dtype = tf.float64):
        data_model = []
        for i in range(len(s2_list)):
            data_model.append((s2_list[i], r_list[i]))
        lists = zip(*data_model)
        s2_tens, r_tens = [tf.convert_to_tensor(x, dtype=dtype) for x in lists]

        timestamp = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

        model = self._define_reward_mapping_model([20,20])

        history = model.fit(
            x=s2_tens,
            y=r_tens,
            validation_split=0.2,
            verbose=1,
            epochs=200,
            callbacks=[
                # tf.keras.callbacks.EarlyStopping(
                #     monitor='val_loss',
                #     mode='min',
                #     patience=5,
                # ),
                tf.keras.callbacks.ModelCheckpoint(
                    filepath=os.path.join('models', f'AWAKE_{timestamp}_model_reward.ckpt'),
                    save_weights_only=True,
                    verbose=1,
                ),
            ],
        )

        plt.plot(history.history['loss'], label='loss_reward')
        plt.plot(history.history['val_loss'], label='val_loss_reward')
        plt.xlabel('# epoch')
        plt.ylabel('error')
        plt.legend()
        plt.pause(1)

        model.save(os.path.join('models', f'AWAKE_{timestamp}_model_reward.h5'))

        return model

