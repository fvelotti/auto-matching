from stable_baselines.common.policies import MlpPolicy
from stable_baselines import TRPO
from awake_env.tl_opt_env_RL import tlOptEnv as environment

import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'


state_type, data_type, state_dof = 'implicit', 'vae', 5

encoder_type = 'fixed'

env = environment(None, state_type=state_type,
                  data_type=data_type, state_dof=state_dof,
                  encoder_type=encoder_type, action_space_type='sb')

logdir='data/' + env.now_str_all
os.mkdir(logdir)

n_actions = env.action_space.shape[-1]

env.action_scaling = 0.1
env.plot_freq = 20
env.save_freq = 200
env.reward_fraction_sigma = 0.8
env.rand_x0 = [0.05,0.05,0.05,0.05,0.05]
env.reward_target = -0.3
env.max_episode_length = 10
env.i0 = 1.3e6

total_timesteps = 1000
learning_starts = 100

hyp_params = {'rl_agent': 'SB PPO', 'learning_starts': learning_starts, 'total_timesteps': total_timesteps,
              'action_scaling': env.action_scaling, 'reward_fraction_sigma': env.reward_fraction_sigma,
              'reward_target': env.reward_target
              }

env.save_settings(additional_log=hyp_params)

model = TRPO(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=1000)
