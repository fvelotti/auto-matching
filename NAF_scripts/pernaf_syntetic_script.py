from awake_env.tl_opt_env_RL import tlOptEnv as environment
from pernaf.naf import NAF
from pernaf.utils.statistic import Statistic
import tensorflow as tf
import matplotlib.pyplot as plt
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

folder_name = 'test_NAF_explicit_5d_synthetic_noise/'
directory = "checkpoints/" + folder_name

# How the raward is changed in feature
discount = 0.999

batch_size = 10
learning_rate = 1e-3

# Max len of episode - not used if max episode len in env
max_steps = 30

# experience replay size
update_repeat = 7
# number of episods
max_episodes = 300

# Target network weight transfer => percentage of how much goes into the second net
tau = 1 - 0.999

is_train = True

# Uses what's inside the folder and starts from there
is_continued = False

# Init for the internal NN
nafnet_kwargs = dict(hidden_sizes=[32, 32], activation=tf.nn.tanh
                     , weight_init=tf.random_uniform_initializer(-0.05, 0.05))

# Priority of replay buffer => to be seen what this means exactly
prio_info = dict(alpha=.5, beta=.5)

noise_info = dict(noise_function=lambda nr: max(0, (1-nr/100)))

with tf.Session() as sess:
    state_type, data_type, state_dof = "explicit", 'vae', 5
    encoder_type = 'fixed'

    env = environment(sess, state_type=state_type, data_type=data_type, state_dof=state_dof,
                      encoder_type=encoder_type)
    env.action_scaling = 0.06
    env.plot_freq = 20
    env.save_freq = 200
    env.reward_fraction_sigma = 0.6
    env.rand_x0 = [0.02, 0.02, 0.02, 0.02, 0.02]
    env.reward_target = -0.2

    hyp_para = {'rl_agent': 'PERNAF', 'folder': folder_name, 'discount': discount, 'batch_size': batch_size,
                'learning_rate': learning_rate, 'max_steps': max_steps, 'update_rep': update_repeat,
                'max_episodes': max_episodes, 'tau': tau, 'is_train': is_train, 'is_continued': is_continued,
                'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma,
                'target': env.reward_target}

    env.save_settings(additional_log=hyp_para)

    # statistics and running the agent
    stat = Statistic(sess=sess, env_name=env.__name__, model_dir=directory,
                     max_update_per_step=update_repeat, is_continued=is_continued, save_frequency=5)
    print('init the agent')
    agent = NAF(sess=sess, env=env, stat=stat, discount=
    discount, batch_size=batch_size,
                learning_rate=learning_rate, max_steps=max_steps, update_repeat=update_repeat,
                max_episodes=max_episodes, tau=tau, pretune = None, prio_info=prio_info, noise_info=noise_info,
                **nafnet_kwargs)

    init_op = tf.global_variables_initializer()  # create the graph
    sess.run(init_op)

    if data_type == 'vae':
        print('loaded 1')
        env.model.model_btv._loadFiles()

    if state_type == 'implicit':
        print('loaded 2')
        env.stateEncoder._loadFiles()
    # run the agent
    agent.run(is_train)

env.fig.show()
env.fig.savefig('/Users/fvelotti/Dropbox/Awake/Plots/naf_auto_matching/' + hyp_para['rl_agent'] + '_actions_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig2.show()

env.fig2.savefig('/Users/fvelotti/Dropbox/Awake/Plots/naf_auto_matching/' + hyp_para['rl_agent'] +
                '_all_interactions_' + env.now_str_all + '.pdf')
plt.pause(1)

env.fig3.show()
env.fig3.savefig('/Users/fvelotti/Dropbox/Awake/Plots/naf_auto_matching/' + hyp_para['rl_agent'] + '_stats_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig5.show()

env.fig5.savefig('/Users/fvelotti/Dropbox/Awake/Plots/naf_auto_matching/' + hyp_para['rl_agent'] + '_states_' +
                env.now_str_all + '.pdf')
plt.pause(1)