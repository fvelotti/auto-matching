from awake_env.tl_opt_env_RL import tlOptEnv as environment
from pernaf.naf import NAF
from pernaf.utils.statistic import Statistic
import tensorflow as tf

file_name = 'test_NAF_explicit_5d_machine/'
directory = "checkpoints/" + file_name

# How the raward is changed in feature
discount = 0.999

batch_size = 10
learning_rate = 1e-3

# Max len of episode - not used if max episode len in env
max_steps = 30

# experience replay size
update_repeat = 7
# number of episods
max_episodes = 300

# Target network weight transfer => percentage of how much goes into the second net
tau = 1 - 0.999

is_train = True

# Uses what's inside the folder and starts from there
is_continued = False

# Init for the internal NN
nafnet_kwargs = dict(hidden_sizes=[32, 32], activation=tf.nn.tanh
                     , weight_init=tf.random_uniform_initializer(-0.05, 0.05))

# Priority of replay buffer => to be seen what this means exactly
prio_info = dict(alpha=.5, beta=.5)

# filename = 'Scan_data.obj'
# filehandler = open(filename, 'rb')
# scan_data = pickle.load(filehandler)
#
#env = environment(None)
#env.reset()
#env.step(env.normData(np.array([119.05946208, 159.78152425,   0.94815722,  -5.21167711, -25.97370551])))


with tf.Session() as sess:
    state_type, data_type, state_dof = "explicit", 'btv', 5
    encoder_type = 'fixed'
    env = environment(sess, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type)
    env.action_scaling = 0.06
    env.plot_freq = 1

    # statistics and running the agent
    stat = Statistic(sess=sess, env_name=env.__name__, model_dir=directory,
                     max_update_per_step=update_repeat, is_continued=is_continued, save_frequency=5)
    print('init the agent')
    agent = NAF(sess=sess, env=env, stat=stat, discount=
    discount, batch_size=batch_size,
                learning_rate=learning_rate, max_steps=max_steps, update_repeat=update_repeat,
                max_episodes=max_episodes, tau=tau, pretune = None, prio_info=prio_info, **nafnet_kwargs)
    init_op = tf.global_variables_initializer()  # create the graph
    sess.run(init_op)

    if data_type == 'vae':
        print('loaded 1')
        env.model.model_btv._loadFiles()

    if state_type == 'implicit':
        print('loaded 2')
        env.stateEncoder._loadFiles()
    # run the agent
    agent.run(is_train)

