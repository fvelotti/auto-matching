import pickle
import matplotlib.pyplot as plt
import pandas as pd
import os
from pathlib import Path
import numpy as np

ROOT_DATA = Path("/home/fvelotti/pCloudDrive/Awake/Data/")
# ROOT_DATA = Path("/home/five loti/Downloads/")

# INNER_FOLDER = ""
INNER_FOLDER_AUG = "Scans_Aug_2020"
INNER_FOLDER_JUL = "Scans_JuneJuly_2020"
# Load data

stats_fake = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_20_14_29_btv_fake_enc/"
            "train_stats_2020-07-07_20_14_37.826618_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_fake_train = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_20_14_29_btv_fake_enc/"
            "train_stats_2020-07-07_20_51_33.880342_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_fake_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_20_14_29_btv_fake_enc/"
            "train_stats_2020-07-07_21_19_10.480633_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_implicit = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_15_07_14_btv_5d_imp/"
            "train_stats_2020-08-10_15_07_14.900352_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_implicit_train = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_15_07_14_btv_5d_imp/"
            "train_stats_2020-08-10_15_39_42.539555_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_implicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_15_07_14_btv_5d_imp/"
            "train_stats_2020-08-10_16_02_34.394866_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_explicit = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_18_41_47_btv/"
            "train_stats_2020-07-07_18_41_47.329322_btv_implicit3.p"
        ),
        "rb",
    )
)

stats_explicit_train = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_18_41_47_btv/"
            "train_stats_2020-07-07_19_19_12.751239_btv_implicit3.p"
        ),
        "rb",
    )
)

stats_explicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_18_41_47_btv/"
            "train_stats_2020-07-07_19_48_10.075874_btv_implicit3.p"
        ),
        "rb",
    )
)


def make_df_from_dict(input_dict_stats):
    df_implicit = pd.DataFrame(input_dict_stats).T
    df_implicit["len"] = df_implicit["total"].apply(lambda x: len(x))
    df_implicit["intensity0"] = df_implicit["intensity"].apply(lambda x: x[0])
    df_implicit["size0"] = df_implicit["size"].apply(lambda x: x[0])
    df_implicit["total0"] = df_implicit["total"].apply(lambda x: x[0])
    df_implicit["intensity"] = df_implicit["intensity"].apply(lambda x: x[-1])
    df_implicit["size"] = df_implicit["size"].apply(lambda x: x[-1])
    df_implicit["total"] = df_implicit["total"].apply(lambda x: x[-1])
    df_implicit["delta_rew"] = df_implicit.apply(
        lambda x: x.total - x.total0, axis=1
    )
    return df_implicit


df_explicit = make_df_from_dict(stats_explicit)
df_explicit_train = make_df_from_dict(stats_explicit_train)
df_explicit_valid = make_df_from_dict(stats_explicit_validation)

df_implicit = make_df_from_dict(stats_implicit)
df_implicit_train = make_df_from_dict(stats_implicit_train)
df_implicit_valid = make_df_from_dict(stats_implicit_validation)

df_fake = make_df_from_dict(stats_fake)
df_fake_train = make_df_from_dict(stats_fake_train)
df_fake_valid = make_df_from_dict(stats_fake_validation)

fig, axis = plt.subplots(nrows=2, ncols=1, figsize=(3, 4))
axis[0].plot(df_implicit.disc, label="Implicit state encoding")
axis[0].plot(df_explicit.disc, label="Explicit state encoding")
axis[0].plot(df_fake.disc, label="Fully synthetic data")
axis[0].set(ylabel=r"$r_{ep}$ (arb. u.)", xlabel="Episode #", ylim=(-75, 1))
axis[0].annotate("(a)", xy=(75, -40))
axis[1].plot(df_implicit_train.disc, label="Implicit state encoding")
axis[1].plot(df_explicit_train.disc, label="Explicit state encoding")
axis[1].plot(df_fake_train.disc, label="Fully synthetic data")
axis[1].set(ylabel=r"$r_{ep}$ (arb. u.)", xlabel="Episode #", ylim=(-75, 1))
axis[1].legend()
axis[1].annotate("(b)", xy=(30, -40))
fig.savefig("/home/fvelotti/awake_prab_paper/images/training_atp_all.pdf")
plt.show()

fig, axis = plt.subplots(nrows=2, ncols=1, figsize=(3, 4))
axis[0].plot(df_implicit.len, label="Implicit state encoding")
axis[0].plot(df_explicit.len, label="Explicit state encoding")
axis[0].plot(df_fake.len, label="Fully synthetic data")
axis[0].set(ylabel=r"Episode length", xlabel="Episode #")
axis[0].annotate("(a)", xy=(75, 40))
axis[1].plot(df_implicit_train.len, label="Implicit state encoding")
axis[1].plot(df_explicit_train.len, label="Explicit state encoding")
axis[1].plot(df_fake_train.len, label="Fully synthetic data")
axis[1].set(ylabel=r"Episode length", xlabel="Episode #")
axis[1].legend()
axis[1].annotate("(b)", xy=(30, 40))
fig.savefig("/home/fvelotti/awake_prab_paper/images/training_len_all.pdf")
plt.show()

fig, axis = plt.subplots(nrows=2, ncols=1, figsize=(3, 4))
axis[0].plot(
    df_implicit_train.total - df_implicit_train.total0,
    label="Implicit state encoding",
)
axis[0].plot(
    df_explicit_train.total - df_explicit_train.total0,
    label="Explicit state encoding",
)
axis[0].plot(
    df_fake_train.total - df_fake_train.total0, label="Fully synthetic data"
)
axis[0].set(ylabel=r"$\Delta r_{ep}$", xlabel="Episode #", ylim=(-1, 1))
axis[0].axhline(0, ls="--")
axis[0].legend()
axis[0].annotate("(a)", xy=(30, 0.8))
axis[1].plot(
    df_implicit_valid.total - df_implicit_valid.total0,
    label="Implicit state encoding",
)
axis[1].plot(
    df_explicit_valid.total - df_explicit_valid.total0,
    label="Explicit state encoding",
)
axis[1].plot(
    df_fake_valid.total - df_fake_valid.total0, label="Fully synthetic data"
)
axis[1].set(ylabel=r"$\Delta r_{ep}$", xlabel="Episode #", ylim=(-1, 1))
axis[1].axhline(0, ls="--")
axis[1].legend()
axis[1].annotate("(b)", xy=(20, 0.8))
fig.savefig("/home/fvelotti/awake_prab_paper/images/validation_all.pdf")
plt.show()