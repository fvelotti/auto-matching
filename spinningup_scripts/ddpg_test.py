from awake_env.tl_opt_env_RL import tlOptEnv as environment

from spinup.utils.test_policy import load_policy, run_policy

state_type, data_type, state_dof = "explicit", 'btv', 5
encoder_type = 'fixed'
random_seed = 10
env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type,
                  seed=random_seed)

env.action_scaling = 0.06
env.plot_freq = 1
env.save_freq = 10
env.reward_fraction_sigma = 0.6
env.rand_x0 = [0.02, 0.02, 0.02, 0.02, 0.02]
env.reward_target = -0.3

env.max_episode_length = 30

_, get_action = load_policy('./data/ddpg_2019-12-01_12_09_31.636838')
run_policy(env, get_action, max_ep_len=30, num_episodes=20)