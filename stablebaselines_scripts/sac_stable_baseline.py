from stable_baselines.sac.policies import MlpPolicy
from stable_baselines import SAC
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import os

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = "explicit", 'vae', 5
encoder_type = 'fixed'
random_seed = 10

env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type,
                  seed=random_seed, action_space_type='sb')

env.action_scaling = 0.06
env.plot_freq = 20
env.save_freq = 200
env.reward_fraction_sigma = 0.6
env.rand_x0 = [0.02, 0.02, 0.02, 0.02, 0.02]
env.reward_target = -0.25

env.max_episode_length = 20

total_timestamps, start_steps = 600, 150
act_noise = 0.03


hyp_para = {'rl_agent': 'SAC', 'total_timestamps': total_timestamps, 'start_steps': start_steps,
            'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma, 'act_noise':
                act_noise, 'target': env.reward_target, 'random_seed': random_seed}

env.save_settings(additional_log=hyp_para)

logger_kwargs = dict(output_dir='data/sac_' + env.now_str_all, exp_name='sac_am_' + env.now_str_all)

model = SAC(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=total_timestamps, log_interval=10)
