import pickle
import matplotlib.pyplot as plt
import pandas as pd
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import os
import numpy as np
import h5py

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"


# Load data
data = h5py.File(
    "./data/2020-10-02_12_05_42_btv_optimiser_150pC_btv54/settings_2020-10-02_12_05_42.070884_btv_implicit.p",
    "r",
)

data = data["data"]
columns = data["block0_items"].value
columns = list(map(lambda x: x.decode("utf-8"), columns))
values = data["block0_values"].value

data_all = pd.DataFrame(values, columns=columns)

s1, s2, q1, q2, q3 = columns[1:6]

fig, axis = plt.subplots(
    2,
    1,
    figsize=(3, 2.7),
    sharex=True,
    gridspec_kw={"height_ratios": [1, 3]},
)

axis[0].plot(data_all[columns[3:6]])
axis[0].set_ylabel(r"K ($m^{-1}$)", fontsize=7)

axis[-1].plot(data_all["penalty"])
# axis[-1].set_yscale('log')
axis[1].set_ylabel("Penalty (arb. u.)", fontsize=7)
axis[1].set_xlabel("Iterations")
axis[1].axhline(0, color="k", ls="--", alpha=0.5)

fig.savefig(
    "/home/fvelotti/pCloudDrive/Awake/Plots/ML_paper/optmiser_3dof.pdf"
)
fig.savefig("/home/fvelotti/awake_prab_paper/images/optmiser_3dof.pdf")
plt.show()