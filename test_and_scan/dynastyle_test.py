from matplotlib import pyplot as plt
import numpy as np
import pickle
import os
from keras.models import load_model
from trained_model import trainedmodel

import tensorflow as tf
import pandas as pd
from stable_baselines import TD3
from cern_dynastyle import DynastyleAgent, DynaCallback, collect_random_data, stop_collecting_on
from cern_dynastyle.plotting import HistoryPlot
from cern_awake_mltools.wrappers import LogRewards
from gym.wrappers import TimeLimit
from awake_env.tl_opt_env_RL import tlOptEnv as environment
from stable_baselines.ddpg.noise import NormalActionNoise
import datetime
import logging

logging.basicConfig(level=logging.INFO)


tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

state_type, data_type, state_dof = "implicit", "vae", 5

encoder_type = "fixed"
random_seed = 0

action_scaling = 0.5
plot_freq = 2
no_plot = False
save_freq = 100
reward_fraction_sigma = 0.6
rand_x0 = [0.1, 0.1, 0.1, 0.1, 0.1]
max_episode_length = 50

reward_target_start = -0.43
learning_starts = 100

x0_scaling = 1

now = (
    str(datetime.datetime.now())
    .replace(" ", "_")
    .replace(":", "_")
    .split(".")[0]
)

logdir = f"data/{now}_{data_type}_5d_imp_dyana/"
os.mkdir(logdir)

log_path = {"path": logdir}

env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
    no_plot=no_plot,
    log_path=log_path,
)

n_actions = env.action_space.shape[-1]

env.action_scaling = action_scaling

env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0
env.x0_scaling = x0_scaling
env.set_x0()

env.max_episode_length = max_episode_length

env.reward_target = reward_target_start

if not os.path.isfile(f"./data/data_dynastyle_{data_type}.p"):
    data_initial = collect_random_data(env, 100//2, maxlen=2)
    with open(f"./data/data_dynastyle_{data_type}.p", "wb") as f:
        pickle.dump(data_initial, f)
else:
    with open(f"./data/data_dynastyle_{data_type}.p", "rb") as f:
        data_initial = pickle.load(f)

class EvalCallback(DynaCallback):
    def __init__(self):
        self.history_plot = HistoryPlot()
        self.on_collect_data_step = stop_collecting_on(
            max_episode_steps=20,
            min_cumulative_reward=-5,
        )

    def on_train_model_end(self, i, history):
        self.history_plot.update(history)
        self.history_plot.figure.canvas.draw()

    def on_train_agent_end(self, i, model_env):
        model_env.render()

    def on_collect_data_end(self, i, batch):
        total_rewards = []
        episode_lengths = []
        current_length = 0
        current_cum_reward = 0.0
        for step in batch:
            current_length += 1
            current_cum_reward += step.reward
            print('step:', step.reward)
            if step.done:
                episode_lengths.append(current_length)
                total_rewards.append(current_cum_reward)
                current_length = 0
                current_cum_reward = 0.0
        # evaluate
        print(episode_lengths, total_rewards)
        success_rate = np.mean([
            length < 10 and cum_reward > 0.0 for length, cum_reward in zip(episode_lengths, total_rewards)
        ])
        print('success rate:', success_rate)
        return success_rate >= 0.9


action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))
agent = DynastyleAgent(TD3, "MlpPolicy", env, learning_starts=100, action_noise=action_noise)
def wrap_model_env(model_env):

    old_step = model_env.step
    def step(action):
        obs, reward, _, info = old_step(action)
        reward = np.clip(reward, -1.0, 20.0)
        done =  not (-1.0 < reward < 20.0)
        print('AAAAAAAAA', reward)
        return obs, reward, done, info
    model_env.step = step

    model_env = TimeLimit(model_env, agent.max_episode_steps)
    model_env = LogRewards(model_env)
    return model_env

agent.wrap_model_env = wrap_model_env
agent.learn(
    epochs=10,
    callback=EvalCallback(),
    initial_buffer=data_initial,
    n_timesteps_agent=500,
    n_epochs_model=400,
    hidden_sizes=[128, 128],
)

print('done')

env.update_all_plots()

env.fig.show()
plt.pause(1)
env.fig.savefig(logdir + "/actions" + env.now_str_all + ".png")

env.fig2.show()
plt.pause(1)
env.fig2.savefig(
    logdir + "/all_interactions_" + env.now_str_all + ".png"
)

env.fig3.show()
plt.pause(1)
env.fig3.savefig(logdir + "/stats_" + env.now_str_all + ".png")

env.fig5.show()
plt.pause(1)
env.fig5.savefig(logdir + "/states_" + env.now_str_all + ".png")

plt.show()