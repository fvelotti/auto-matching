import pickle
import matplotlib.pyplot as plt
import pandas as pd
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import numpy as np
from sklearn.ensemble import IsolationForest
# os.environ['KMP_DUPLICATE_LIB_OK']='True'

# %% ---------------------------------------
def get_data(file_name):
    # Load data
    data = pickle.load(open(file_name, 'rb'))
    data = pd.DataFrame(data)

    # Instantiate env object for normalisation purposes
    state_type, data_type, state_dof = 'implicit', 'vae', 5

    encoder_type = 'fixed'
    random_seed = 0

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb')

    env.reward_fraction_sigma = 0.6

    data['is_episode_over'] = data.apply(lambda x: np.any((env.normData(x[env.devices]) > 1)) or
           np.any(env.normData(x[env.devices]) < 0) or x['penalty'] < -env.reward_target, axis=1)

    data['size_r'] = data.apply(lambda x: (env.r0 - np.sqrt(np.sum(
        np.array((x['btv_data']['sigma_x'], x['btv_data']['sigma_y'])) ** 2))) / env.r_max, axis=1)

    data['int_r'] = data.apply(lambda x: (np.sum(x['btv_data']['image']) - env.i0) / env.i0, axis=1)

    data['reward'] = data.apply(lambda x: x['size_r'] * env.reward_fraction_sigma + x['int_r'] * (1 -
                                                                                env.reward_fraction_sigma), axis=1)

    devs_real = [ele + '_real' for ele in env.devices]

    data_real = np.zeros((len(data), len(env.devices)))
    for i in range(len(data)):
        if np.max(data[env.devices[0]]) < 5:
            data_real[i, :] = env.invNormData(data[env.devices].iloc[i])
        else:
            data_real[i, :] = data[env.devices].iloc[i]

    for i, ele in enumerate(devs_real):
        data[ele] = data_real[:, i]

    return data, env


# %% ---------------------------------------
data_file = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/data_scan_2019-11-21_18_41_51.161109_btv_explicit.p'

data, env = get_data(data_file)

# %% ---------------------------------------
file_test = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/data_scan_2019-11-22_06_06_11.629881_btv_explicit.p'

data_test, _ = get_data(file_test)

# %% ---------------------------------------
file_val = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/data_scan_2019-11-23_09_28_41.406639_btv_implict.p'

data_val, _ = get_data(file_val)
# %% ---------------------------------------
treshold = env.reward_target

plt.figure()
plt.plot(data['reward'], '-o', color='k', alpha=0.3)
plt.plot(data['size_r'], '.')
plt.plot(data['int_r'], '.')
plt.axhline(treshold, ls='--')
plt.ylim(-2, 0)
plt.show()

plt.figure()
plt.title('TEST')
plt.plot(data_test['reward'], '-o', color='k', alpha=0.3)
plt.plot(data_test['size_r'], '.')
plt.plot(data_test['int_r'], '.')
plt.axhline(treshold, ls='--')

plt.ylim(-2, 0)
plt.show()

plt.figure()
plt.title('Validation')
plt.plot(data_val['reward'], '-o', color='k', alpha=0.3)
plt.plot(data_val['size_r'], '.')
plt.plot(data_val['int_r'], '.')
plt.axhline(treshold, ls='--')

plt.ylim(-2, 0)
plt.show()


# %% ---------------------------------------
devs_real = [ele + '_real' for ele in env.devices]

plt.figure(figsize=(3, 6))
for i in range(5):
    plt.subplot(5, 1, i + 1)
    plt.plot(data.iloc[:][devs_real[i]])
    plt.ylabel(f'X$_{i}$')
plt.xlabel('Iterations')
plt.show()

# %% ---------------------------------------
# Making data for IF training
X_train = data[devs_real + ['size_r', 'int_r', 'reward']]

X_test = data_test[devs_real + ['size_r', 'int_r', 'reward']]

X_val = data_val[devs_real + ['size_r', 'int_r', 'reward']]

X_train = pd.concat([X_train, X_val], ignore_index=True)
# %% ---------------------------------------
rng = np.random.RandomState(42)
clf = IsolationForest(n_estimators=100, max_samples='auto', random_state=0, contamination=0.04)
clf.fit(X_train)

pickle.dump(clf, open('/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/isolation_forest_nov.pkl', 'wb'))
# %% ---------------------------------------

del clf
clf = pickle.load(open('/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/isolation_forest_nov.pkl', 'rb'))
y_pred_train = clf.predict(X_train)
y_pred_test = clf.predict(X_test)
y_pred_val = clf.predict(X_val)

print(list(y_pred_train).count(-1)/y_pred_train.shape[0])
print(list(y_pred_test).count(-1)/y_pred_test.shape[0])

plt.figure()
plt.title('Training set')
plt.plot(data['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data['size_r'], '.', label='r$_\sigma$')
plt.plot(data['int_r'], '.', label='r$_I$')

plt.plot(y_pred_train, 'o', label='prediction')
plt.axhline(treshold, ls='--')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()

plt.figure()
plt.title('Test set')
plt.plot(data_test['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data_test['size_r'], '.', label='r$_\sigma$')
plt.plot(data_test['int_r'], '.', label='r$_I$')
plt.axhline(treshold, ls='--')
plt.plot(y_pred_test, 'o', label='prediction')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()

plt.figure()
plt.title('Test set2')
plt.plot(data_val['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data_val['size_r'], '.', label='r$_\sigma$')
plt.plot(data_val['int_r'], '.', label='r$_I$')
plt.axhline(treshold, ls='--')
plt.plot(y_pred_val, 'o', label='prediction')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()

# %% ---------------------------------------


def fix_prediction(y_pred, delta=10):
    new_pred = np.zeros_like(y_pred)
    for i in range(0, len(y_pred), delta):

        data_sub = y_pred[i:i + delta]
        mean = np.mean(data_sub)
        if mean > 0.2:
            new_pred[i:i + delta] = np.ones_like(data_sub)
        elif -0.2 <= mean <= 0.2:
            new_pred[i:i + delta] = data_sub
        else:
            new_pred[i:i + delta] = np.zeros_like(data_sub) - 1
    return new_pred

# %% ---------------------------------------
plt.figure()
plt.title('Training set')
plt.plot(data['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data['size_r'], '.', label='r$_\sigma$')
plt.plot(data['int_r'], '.', label='r$_I$')

plt.plot(fix_prediction(y_pred_train), 'o', label='prediction')
plt.axhline(treshold, ls='--')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()

plt.figure()
plt.title('Test set')
plt.plot(data_test['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data_test['size_r'], '.', label='r$_\sigma$')
plt.plot(data_test['int_r'], '.', label='r$_I$')
plt.axhline(treshold, ls='--')
plt.plot(fix_prediction(y_pred_test), 'o', label='prediction')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()

plt.figure()
plt.title('Test set2')
plt.plot(data_val['reward'], '-o', color='k', alpha=0.3, label='reward')
plt.plot(data_val['size_r'], '.', label='r$_\sigma$')
plt.plot(data_val['int_r'], '.', label='r$_I$')
plt.axhline(treshold, ls='--')
plt.plot(fix_prediction(y_pred_val), 'o', label='prediction')
plt.ylim(-2, 1)
plt.legend(frameon=True)
plt.ylabel('Reward')
plt.show()