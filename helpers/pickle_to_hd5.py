import os
import pandas as pd
import pickle

files_p = os.listdir('../data')


def read_data(file_name):
    data = pickle.load(open(file_name, 'rb'))
    data_df = pd.DataFrame(data, index=data['time'])
    data_df['image'] = data_df['btv_data'].apply(lambda x: x['image'])
    data_df['image_x'] = data_df['btv_data'].apply(lambda x: x['x'])
    data_df['image_y'] = data_df['btv_data'].apply(lambda x: x['y'])
    print(len(data_df.keys()))
    return data_df


pickle_files = []
for ele in files_p:
    print(ele)
    if '.p' in ele:
        pickle_files.append(ele)

for ele in pickle_files:
    data = read_data('./data/' + ele)
    print('./data/' + ele[:-2] + '.h5')
    data.to_hdf('./data/' + ele[:-2] + '.h5', key='data', mode='w')
