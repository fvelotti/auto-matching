"""
Read BTV data, plot and save (modified script from Spencer)
@author: Francesco Velotti
"""
import pyjapc
import numpy as np
import matplotlib.pyplot as plt
import time
import os
import sys
import h5py
import datetime

os.environ['AAT'] = '/user/awakeop/AWAKE_ANALYSIS_TOOLS/'
sys.path.append(os.environ['AAT']+'analyses/')
import frame_analysis as fa
# %% ---------------------------------------

#image_source = 'TT43.BTV.430106'
image_source = 'BTV54'
#image_source = 'TT41.BTV.412350.LASER'

path = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/'
folder_data = path + 'btv_meas_oct19/emit_meas_1/'

btv_name = image_source

if 'BTV54' == image_source:
    image_source = 'BOVWA.11TCC4.AWAKECAM11/CameraImage'
    acquisition = '#image'

    height = '#height'
    width = '#width'
    #pix_size = 0.134
    pix_size = '#pixelSize'
elif 'BTV42' == image_source:
    image_source = 'BOVWA.02TCV4.CAM9/CameraImage'
    acquisition = '#image'

    height = '#height'
    width = '#width'
    #pix_size = 0.125 * 5.0
    pix_size = '#pixelSize'

else:
    acquisition = '/Image#imageSet'
    axis1 = '/Image#imagePositionSet1'
    axis2 = '/Image#imagePositionSet2'
    height = '/Image#nbPtsInSet1'
    width = '/Image#nbPtsInSet2'


image = japc.getParam(image_source+acquisition, timingSelectorOverride='SPS.USER.SFTPRO2')

# %% ---------------------------------------

if 'BOVWA' in image_source:
    w = japc.getParam(image_source+height, timingSelectorOverride='SPS.USER.SFTPRO2')
    h = japc.getParam(image_source+width, timingSelectorOverride='SPS.USER.SFTPRO2')
    if btv_name == 'BTV54':
        p = 0.134/5.0   
    else:
        p = japc.getParam(image_source+pix_size, timingSelectorOverride='SPS.USER.SFTPRO2')        
    
else:
    im_ax1 = japc.getParam(image_source+axis1, timingSelectorOverride='SPS.USER.SFTPRO2')
    im_ax2 = japc.getParam(image_source+axis2, timingSelectorOverride='SPS.USER.SFTPRO2')
    h = japc.getParam(image_source+height, timingSelectorOverride='SPS.USER.SFTPRO2')
    w = japc.getParam(image_source+width, timingSelectorOverride='SPS.USER.SFTPRO2')

    image = np.reshape(image,(w,h))

if 'BOVWA' in image_source:
#    if btv_name == 'BTV42':
        
#     im_ax1 = np.linspace(-w/2, w/2, w) * p*5.0
#     im_ax2 = np.linspace(-h/2, h/2, h) * p*5.0
    im_ax1 = np.linspace(-h/2, h/2, h) * p*5.0
    im_ax2 = np.linspace(-w/2, w/2, w) * p*5.0
    image = image
#     else:
#         im_ax1 = np.linspace(-h/2, h/2, h) * p*5.0
#         im_ax2 = np.linspace(-w/2, w/2, w) * p*5.0

#roi = [float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5])]

# if btv_name == 'BTV54':
#     roi = np.array([min(im_ax1), max(im_ax1), min(im_ax2), max(im_ax2)]) * 0.8
# else:
#     roi = [min(im_ax1), max(im_ax1), min(im_ax2), max(im_ax2)]

pro_x = image.sum(axis=1)
pro_y = image.sum(axis=0)

x_min = im_ax1[np.argmax(pro_x)] - 15
x_max = im_ax1[np.argmax(pro_x)] + 15
y_min = im_ax2[np.argmax(pro_y)] - 15
y_max = im_ax2[np.argmax(pro_y)] + 15

roi = np.array([x_min, x_max, y_min, y_max])
    
frame_ana = fa.FrameAna(image,im_ax1,im_ax2,roi)

frame_ana.fit_gauss = True

frame_ana.analyze_frame()

# %% ---------------------------------------
nShot = 2
x_prof_array = np.zeros((nShot, frame_ana.frame.shape[1]))
y_prof_array = np.zeros((nShot, frame_ana.frame.shape[0]))

x_fit_array = np.zeros((nShot, frame_ana.frame.shape[1]))
y_fit_array = np.zeros((nShot, frame_ana.frame.shape[0]))

x_rms = np.zeros(nShot)
y_rms = np.zeros(nShot)

x_bar = np.zeros(nShot)
y_bar = np.zeros(nShot)

x_sigma = np.zeros(nShot)
y_sigma = np.zeros(nShot)

x_mu = np.zeros(nShot)
y_mu = np.zeros(nShot)
# %% ---------------------------------------
# plt.figure()
# t_string = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_')
# plt.title(t_string)
# plt.imshow(image)
# plt.savefig(folder_data + '/image.png')

# %% ---------------------------------------
def getNewImage(param_val):
    global count, image_all, im_ax1, im_ax2, roi, x_prof_array, y_prof_array, x_fit_array, y_fit_array, x_rms, y_rms, x_sigma, y_sigma
    print('Got Image')
    time.sleep(1.2)
    if count < nShot:
        if not "BOVWA" in image_source:
            image = np.reshape(param_val,(w, h))
        else:
            image = param_val
        
        #frame_ana = fa.FrameAna(image,im_ax1,im_ax2,roi)
        print('Before call')
        frame_ana = fa.FrameAna()
        frame_ana.frame = image
        frame_ana.x_ax = im_ax1
        frame_ana.y_ax = im_ax2

        frame_ana.roi = roi

        frame_ana.fit_gauss = True
        frame_ana.analyze_frame()
        if count == 0:
            image_all = frame_ana.frame
        else:
            image_all = frame_ana.frame + image_all

        x_prof_array[count,:] = frame_ana.proj_x
        y_prof_array[count,:] = frame_ana.proj_y
        x_fit_array[count,:] = frame_ana.fit_x
        y_fit_array[count,:] = frame_ana.fit_y
        x_rms[count] = frame_ana.xRMS
        y_rms[count] = frame_ana.yRMS

        x_sigma[count] = frame_ana.sig_x
        y_sigma[count] = frame_ana.sig_y
        x_bar[count] = frame_ana.xBar
        y_bar[count] = frame_ana.yBar
        x_mu[count] = frame_ana.mean_x
        y_mu[count] = frame_ana.mean_y
        print('end')

    else:
        print('Done!')


#%%
for count in range(nShot):
    newImage = japc.getParam(image_source+acquisition, timingSelectorOverride='SPS.USER.SFTPRO2')
    getNewImage(newImage)
#
# input('wait...then enter\n')

#%%
t_string = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_')
file_name = btv_name + '_' + t_string

f = h5py.File(folder_data + '/data_' + file_name + '.h5', 'w')
f.create_dataset('xAxis', data=frame_ana.x_ax)
f.create_dataset('yAxis', data=frame_ana.y_ax)
f.create_dataset('xProfiles', data=x_prof_array)
f.create_dataset('yProfiles', data=y_prof_array)
f.create_dataset('xFits', data=x_fit_array)
f.create_dataset('yFits', data=y_fit_array)
f.create_dataset('xRMS', data=x_rms)
f.create_dataset('yRMS', data=y_rms)
f.create_dataset('xSigma', data=x_sigma)
f.create_dataset('ySigma', data=y_sigma)
f.create_dataset('image', data=image_all / (count + 1))
f.close()
# %% ---------------------------------------
plt.figure()
X, Y = np.meshgrid(frame_ana.x_ax, frame_ana.y_ax[::-1])
plt.contourf(X, Y, image_all / (count + 1))
plt.xlabel('x / mm')
plt.ylabel('y / mm')
plt.savefig(folder_data + '/image_mean' + file_name + '.png')
# %% ---------------------------------------

plt.figure()
plt.plot(frame_ana.x_ax, np.mean(x_prof_array, axis=0), label=r'Xrms = %.2f mm'%np.mean(x_rms))
plt.plot(frame_ana.x_ax, np.mean(x_fit_array, axis=0), label=r'Xfit = %.2f mm'%np.mean(x_sigma))
plt.plot(frame_ana.y_ax, np.mean(y_prof_array, axis=0), label=r'Yrms = %.2f mm'%np.mean(y_rms))
plt.plot(frame_ana.y_ax, np.mean(y_fit_array, axis=0), label=r'Yfit = %.2f mm'%np.mean(y_sigma))
print('rms: \n')
print(np.mean(x_rms))
print(np.mean(y_rms))

print('Gaussian fit: \n')
print(np.mean(x_sigma))
print(np.mean(y_sigma))

plt.legend()
plt.xlabel('x, y / mm')
plt.savefig(folder_data + '/image_proj' + file_name + '.png')

plt.show()
