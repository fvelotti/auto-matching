import pickle
import matplotlib.pyplot as plt
import numpy as np

data = pickle.load(open('data/stability_explicit.p', 'rb'))

plt.figure()
plt.hist(data['r'])
plt.show()

plt.figure()
plt.hist(data['o'][:, 0])
plt.show()