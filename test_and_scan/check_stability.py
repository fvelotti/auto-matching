from awake_env.tl_opt_env_RL import tlOptEnv as environment
import numpy as np

import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'


state_type, data_type, state_dof = "implicit", 'btv', 5
encoder_type = 'fixed'
random_seed = 10


env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type,
                  seed=random_seed, action_space_type='sb')
env.rand_x0 = [0.0, 0.0, 0.0, 0.0, 0.0]
steps = 100
env.plot_freq = 1
env.save_freq = 200
env.reset()
data_stats = {'r': np.zeros(steps), 'o': np.zeros((steps, state_dof))}
for i in range(steps):
    o, r, _, _ = env.step(np.zeros(env.dof))
    data_stats['r'][i] = r
    data_stats['o'][i, :] = o

# pickle.dump(data_stats, open('data/stability_' + state_type + '.p', 'wb'))