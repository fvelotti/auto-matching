import pickle
import tensorflow as tf
import h5py
import os
import numpy as np
# %% ===================================
import matplotlib.pyplot as plt

# %% ===================================
tf_type = tf.float64
logdir = 'auto-matching/data/2020-08-11_16_04_58_btv_5d/'

data = pickle.load(open('auto-matching/data/2020-08-13_18_12_41_btv_5d_btv53/settings_2020-08-13_18_12_42.119443_btv_implicit.p', 'rb'))

states = [f's{i}' for i in range(5)]
actions = ['RPSKN.TSG4.SNH.430000',
           'RPSKN.TSG4.SNJ.430001',
           'logical.RQID.430031',
           'logical.RQIF.430034',
           'logical.RQID.430037']
# %% ===================================

def fake_step(i):
    s0 = [data[state][i] for state in states]
    a = [data[action][i + 1] for action in actions]
    s1 = [data[state][i + 1] for state in states]
    r = -data['penalty'][i + 1]

    return s0, a, s1, r

# %% ===================================
def get_data(n_data, d_type):

    x_list, u_list, y_list, r_list = [], [], [], []
    for i in range(0, n_data, 2):
        s0, a, s1, r1 = fake_step(i)

        x_val = tf.convert_to_tensor(s0, d_type)
        u_val = tf.convert_to_tensor(a, d_type)
        r_val = tf.convert_to_tensor(r1, dtype=tf.float64)
        y_val = tf.convert_to_tensor(s1, dtype=tf.float64)

        x_list.append(x_val)
        u_list.append(u_val)
        y_list.append(y_val)
        r_list.append(r_val)

    return tf.stack(x_list), tf.stack(u_list), tf.stack(y_list), tf.stack(r_list)


def store(filename, group, dataset_list, data_list):
    address = logdir + filename
    if os.path.isfile(address):
        hdf = h5py.File(address, 'a')
    else:
        hdf = h5py.File(address, 'w')
    print(data_list)
    g = hdf.create_group(group)
    for idx in range(len(dataset_list)):
        print(idx)
        g.create_dataset(dataset_list[idx], data=data_list[idx])

    hdf.close()
    return 0

# %% ===================================

num_data = len(data['penalty']) - 1
x_set, u_set, y_set, r_set = get_data(num_data, tf_type)
# store(filename='data_awake_ilqr.hdf5',
#         group='data',
#         dataset_list=['n_data', 'train%', 'x_set', 'u_set', 'y_set', 'r_set'],
#         data_list=[num_data, th_data, x_set, u_set, y_set, r_set])
# %% ===================================
plt.figure()
plt.plot(data['penalty'])
plt.show()

# %% ===================================
pickle.dump((x_set, u_set, y_set, r_set), open(logdir + 'data_awake_ilqr.p', 'wb'))