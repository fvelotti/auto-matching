import pickle
import matplotlib.pyplot as plt
import pandas as pd
import os
from pathlib import Path
import numpy as np

ROOT_DATA = Path("/home/fvelotti/pCloudDrive/Awake/Data/")
# ROOT_DATA = Path("/home/five loti/Downloads/")

# INNER_FOLDER = ""
INNER_FOLDER_AUG = "Scans_Aug_2020"
INNER_FOLDER_JUL = "Scans_JuneJuly_2020"
# Load data

stats_implicit_validation_3 = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-12_09_54_56_btv_val_200pC/"
            "train_stats_2020-08-12_09_54_56.552355_btv_implicit5.p"
        ),
        "rb",
    )
)


stats_explicit_validation_3 = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_11_18_05_btv_val_from_july_val_fake/"
            "train_stats_2020-08-10_11_18_07.586939_btv_implicit5.p"
        ),
        "rb",
    )
)


stats_implicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-09_10_28_38_btv_val_day_after_5d_imp/"
            "train_stats_2020-07-09_10_28_38.969359_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_implicit_validation_2 = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-09_10_48_24_btv_val_day_after_5d_imp/"
            "train_stats_2020-07-09_10_48_24.796029_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_explicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-09_11_03_09_btv_val_day_after_fake_val/"
            "train_stats_2020-07-09_11_03_09.700105_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_explicit_validation_2 = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-09_11_19_33_btv_val_day_after_fake_val/"
            "train_stats_2020-07-09_11_19_35.711739_btv_implicit5.p"
        ),
        "rb",
    )
)

stats_explicit_validation_exp = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_11_28_11_btv_val_from_july_val_expl/"
            "train_stats_2020-08-10_11_28_11.206189_btv_explicit5.p"
        ),
        "rb",
    )
)

def make_df_from_dict(input_dict_stats):
    df_implicit = pd.DataFrame(input_dict_stats).T
    df_implicit["len"] = df_implicit["total"].apply(lambda x: len(x))
    df_implicit["intensity0"] = df_implicit["intensity"].apply(lambda x: x[0])
    df_implicit["size0"] = df_implicit["size"].apply(lambda x: x[0])
    df_implicit["total0"] = df_implicit["total"].apply(lambda x: x[0])
    df_implicit["intensity"] = df_implicit["intensity"].apply(lambda x: x[-1])
    df_implicit["size"] = df_implicit["size"].apply(lambda x: x[-1])
    df_implicit["total"] = df_implicit["total"].apply(lambda x: x[-1])
    df_implicit["delta_rew"] = df_implicit.apply(
        lambda x: x.total - x.total0, axis=1
    )
    return df_implicit


df_explicit_valid = make_df_from_dict(stats_explicit_validation)
df_explicit_valid_2 = make_df_from_dict(stats_explicit_validation_2)
df_explicit_valid_3 = make_df_from_dict(stats_explicit_validation_3)

df_implicit_valid = make_df_from_dict(stats_implicit_validation)
df_implicit_valid_2 = make_df_from_dict(stats_implicit_validation_2)
df_implicit_valid_3 = make_df_from_dict(stats_implicit_validation_3)

df_explicit_valid_exp = make_df_from_dict(stats_explicit_validation_exp)


fig, axis = plt.subplots(nrows=2, ncols=1, figsize=(3, 4))
axis[0].plot(
    df_implicit_valid.total - df_implicit_valid.total0,
    label="Implicit state encoding",
)
axis[0].plot(
    df_implicit_valid_2.total - df_implicit_valid_2.total0, "--", c="C0"
)
axis[0].plot(
    df_implicit_valid_3.total - df_implicit_valid_3.total0, "x-", c="C0"
)
axis[0].plot(
    df_explicit_valid_exp.total - df_explicit_valid_exp.total0, c="C1",
    label="Explicit state encoding",
)

axis[0].plot(
    df_explicit_valid.total - df_explicit_valid.total0, c="C2",
    label="Fully synthetic state encoding",
)
axis[0].plot(
    df_explicit_valid_2.total - df_explicit_valid_2.total0, "--", c="C2",
)
axis[0].plot(
    df_explicit_valid_3.total - df_explicit_valid_3.total0, "x-", c="C2",
)
axis[0].set(ylabel=r"$\Delta r_{ep}$", xlabel="Episode #", ylim=(-1, 1))
axis[0].axhline(0, ls="--", c="k")
axis[0].annotate("(a)", xy=(7.5, 0.8))

axis[1].plot(
    df_implicit_valid.len,
    label="Implicit state encoding",
)
axis[1].plot(
    df_implicit_valid_2.len, "--", c="C0"
)
axis[1].plot(
    df_implicit_valid_3.len, "x-", c="C0"
)
axis[1].plot(
    df_explicit_valid_exp.len, c="C1",
    label="Explicit state encoding",
)

axis[1].plot(
    df_explicit_valid.len, c="C2",
    label="Fully synthetic state encoding",
)
axis[1].plot(
    df_explicit_valid_2.len, "--", c="C2",
)
axis[1].plot(
    df_explicit_valid_3.len, "x-", c="C2",
)
axis[1].set(ylabel=r"Episode length", xlabel="Episode #", ylim=(0, 60))
axis[1].legend()
axis[1].annotate("(b)", xy=(7.5, 4))
fig.savefig("/home/fvelotti/awake_prab_paper/validation_in_time.pdf")
plt.show()