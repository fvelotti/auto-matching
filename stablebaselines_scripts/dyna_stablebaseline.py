import logging
import os

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

from datetime import datetime
import pandas as pd
import numpy as np
from stable_baselines import TD3
from stable_baselines.td3.policies import MlpPolicy
from stable_baselines.common.noise import NormalActionNoise
import tensorflow as tf
from gym.wrappers import TimeLimit
import gym
import pickle

# from envs import env_awake_steering_simulated as awake_sim
from awake_env import env_surrogate

import matplotlib

# print("backend ",matplotlib.get_backend())
# print(matplotlib.rcParams['backend'])
# matplotlib.use('module://backend_interagg')
matplotlib.use('qt5agg')
import matplotlib.pyplot as plt

import pandas as pd


def plot_results(env, label, filename, color_final='blue'):
    rewards = env.rewards
    initial_states = env.initial_conditions

    iterations = []
    finals = []
    starts = []

    for i in range(len(rewards)):
        if (len(rewards[i]) > 0):
            finals.append(rewards[i][len(rewards[i]) - 1])
            starts.append(-np.sqrt(np.mean(np.square(initial_states[i]))))
            iterations.append(len(rewards[i]))

    plot_suffix = f', number of iterations: {env.TOTAL_COUNTER}'

    fig, axs = plt.subplots(2, 1, constrained_layout=True)

    ax = axs[0]
    ax.plot(iterations)
    # ax.set_title('Iterations' + plot_suffix)
    ax.set_title(label, fontsize=12)

    ax = axs[1]
    ax.set_ylabel('Final RMS', color=color_final)
    ax.tick_params(axis='y', labelcolor=color_final)
    ax.plot(finals, color=color_final)
    ax.axhline(env.threshold, ls=':', c='r')
    ax.set_xlabel('Episodes ')

    color_start = 'lime'

    ax.plot(starts, color=color_start)

    # plt.savefig("data/" + filename + ".pdf")
    plt.pause(1)


def get_environment(mode='vae'):

    from awake_env.tl_opt_env_RL import tlOptEnv as environment

    state_type, data_type, state_dof = 'implicit', mode, 7

    encoder_type = 'fixed'
    random_seed = 0

    action_scaling = 0.4
    plot_freq = 2
    no_plot = False
    save_freq = 3
    reward_fraction_sigma = 0.6
    rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
    max_episode_length = 100

    reward_target_start = -0.5
    learning_starts = 100

    x0_scaling = 0.8

    now = str(datetime.now()).replace(' ', '_').replace(':', '_').split(".")[0]
    logdir = f'data/{now}_{data_type}_5d/'
    os.mkdir(logdir)

    new_model = {}
    log_path = {'path': logdir}

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb', reward_dang=False,
                      no_plot=no_plot, log_path=log_path, new_model=new_model)

    env.set_log_path({'path': logdir})

    n_actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

    env.action_scaling = action_scaling
    env.plot_freq = plot_freq
    env.save_freq = save_freq
    env.reward_fraction_sigma = reward_fraction_sigma
    env.rand_x0 = rand_x0
    env.x0_scaling = x0_scaling
    env.set_x0()

    env.max_episode_length = max_episode_length

    env.reward_target = reward_target_start
    env.reward_dang_min = env.reward_target

    hyp_params = {'rl_agent': 'SB TD3', 'learning_starts': learning_starts,
                  'action_scaling': env.action_scaling, 'reward_fraction_sigma': env.reward_fraction_sigma,
                  'action_noise': action_noise, 'reward_target': env.reward_target, 'random_seed': random_seed,
                  }

    env.save_settings(additional_log=hyp_params)

    # env = TimeLimit(env)
    # env = PenalizeFailure(env, 30.0)
    # env = LogEpisodes(env)
    # env = LogRms(env)
    # if not simulation:
    #     env = RenderOnStep(env)
    return env


def store_initial_data(timestamp, s1_list, a_list, s2_list, r_list):
    pickle_filename = f"data/replay_{timestamp}_initial_data.pkl"
    output = open(pickle_filename, 'wb')
    pickle.dump(s1_list, output, -1)
    pickle.dump(a_list, output, -1)
    pickle.dump(s2_list, output, -1)
    pickle.dump(r_list, output, -1)
    output.close()


def read_initial_data(filename):
    input = open(filename, 'rb')
    s1_list = pickle.load(input)
    a_list = pickle.load(input)
    s2_list = pickle.load(input)
    r_list = pickle.load(input)
    input.close()
    return s1_list, a_list, s2_list, r_list


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    timestamp = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    # original
    #  env_real = awake_sim.e_trajectory_simENV()
    #  env_real.MAX_TIME=15

    # new machine environments
    env_real = get_environment(mode='vae')
    env_real.threshold = env_real.reward_target
    env_real.max_episode_length = 20

    # number_bpm_measurements = 30
    # element_actor_list_selected = pd.Series(element_actor_list[:10])
    # element_state_list_selected = pd.Series(element_state_list[:10])  # was 11 for NAF
    # env_real = awakeEnv(action_space=element_actor_list_selected, state_space=element_state_list_selected,
    #                number_bpm_measurements=number_bpm_measurements, debug=True, scale=3e-4)
    # env_real.MAX_TIME = 15

    s0 = np.zeros(env_real.observation_space.shape[-1])
    # s0 = env_real.reset()
    # if use for "dreaming":
    # env_surrogate = env_surrogate.env_surrogate(env_sim, s0)

    # if not for dreaming
    env_surrogate = env_surrogate.env_surrogate(env_real, s0)
    env_surrogate.MAX_TIME = 100

    is_accumulate_data = True
    if (is_accumulate_data):
        s1_list, a_list, s2_list, r_list = env_surrogate.accumulate_data(50)

        store_initial_data(timestamp, s1_list, a_list, s2_list, r_list)
    else:
        filename = 'data/replay_2020_08_11_11_28_57_initial_data.pkl'
        s1_list, a_list, s2_list, r_list = read_initial_data(filename)

    model = env_surrogate.train_model(s1_list, a_list, s2_list)
    model_reward = env_surrogate.train_reward_mapping(s2_list, r_list)

    # model = tf.keras.models.load_model('data/AWAKE_2020_08_05_16_46_17_model.h5', compile=False)
    # model_reward = tf.keras.models.load_model('data/AWAKE_2020_08_05_16_46_21_model_reward.h5', compile=False)

    env_surrogate.set_model(model)

    env_surrogate.set_reward_model(model_reward)

    n_actions = env_surrogate.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

    td3 = TD3(MlpPolicy, env_surrogate, train_freq=10, learning_starts=100, action_noise=action_noise, verbose=1)

    timesteps = 200

    test_rewards = []
    test_steps = []
    for i in range(10):

        print("Training ", i)
        td3.learn(total_timesteps=timesteps, log_interval=10)

        plot_results(env_surrogate, "", f'fig_agent_training_{timestamp}_{i}')

        print("Testing ", i)
        # Model test....
        test_score = []

        test_rewards.append([])
        test_steps.append([])
        for episode in range(10):

            done = False
            steps = 0

            s1 = env_real.reset()  ##

            while (not done):
                action = td3.predict(s1)[0]  # also predicts the next state for recurrent models
                s2, final_reward, done, _ = env_real.step(action)  ##
                print("Reward ", final_reward, done, env_real.threshold)

                steps += 1

                if (final_reward > env_real.threshold):
                    s1_list.append(s1)
                    s2_list.append(s2)
                    a_list.append(action)
                    r_list.append(final_reward)

                s1 = s2

            score = 0
            print(final_reward, steps)
            test_rewards[i].append(final_reward)
            test_steps[i].append(steps)
            if (steps <= 10 and final_reward > env_real.threshold):
                score = 1
            test_score.append(score)
            if (steps >= env_real.MAX_TIME):
                break

        test_score = np.array(test_score)
        print(test_score)
        total_test_score = np.sum(test_score) / len(test_score)

        if (total_test_score >= 0.9):
            td3.save(f'models/AWAKE_{timestamp}_td3')
            break

        # if(s1_list and a_list and s2_list and r_list):
        #     s1_list, a_list, s2_list, r_list = env_surrogate.accumulate_data(10,s1_list=s1_list,s2_list=s2_list,a_list=a_list,r_list=r_list)
        # else:
        #     s1_list, a_list, s2_list, r_list = env_surrogate.accumulate_data(10)

        model = env_surrogate.train_model(s1_list, a_list, s2_list)
        model_reward = env_surrogate.train_reward_mapping(s2_list, r_list)

        env_surrogate.set_model(model)
        env_surrogate.set_reward_model(model_reward)

    print("total number of data points: ", len(s1_list))
    # print("total number of episodes on real environment ",len(env_real.rewards))
    #
    # counter = 0
    # for i in range(len(env_real.rewards)):
    #     counter+=1
    #     if(len(env_real.rewards[i])>0):
    #         for _ in range(len(env_real.rewards[i])):
    #             counter+=1
    #
    # print("total number of interaction with real environment ", counter)

    # plot_results(env_real, "Interaction with machine",f'fig_machine_interaction_{timestamp}.pdf',color_final='magenta')

    print(test_rewards)
    print(test_steps)

    # pickle_filename = f"data/replay_{timestamp}_data.pkl"
    # output = open(pickle_filename, 'wb')
    # pickle.dump(s1_list, output, -1)
    # pickle.dump(a_list, output, -1)
    # pickle.dump(s2_list, output, -1)
    # pickle.dump(r_list, output, -1)
    # output.close()
    #
    # pickle_filename = f"data/score_{timestamp}_data.pkl"
    # output = open(pickle_filename, 'wb')
    # pickle.dump(test_rewards, output, -1)
    # pickle.dump(test_steps, output, -1)
    # output.close()
    #
    # pickle_filename = f"data/agent_training_{timestamp}_data.pkl"
    # output = open(pickle_filename, 'wb')
    # pickle.dump(env_surrogate.rewards, output, -1)
    # pickle.dump(env_surrogate.initial_conditions, output, -1)
    # output.close()

    fig, ax = plt.subplots()
    for i in range(len(test_rewards)):
        plt.plot(test_rewards[i], label='test series' + str(i))
    plt.xlabel("# tests")
    plt.ylabel("- RMS [cm]")
    plt.legend()
    plt.show()
