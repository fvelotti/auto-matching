from stable_baselines.ddpg.policies import MlpPolicy
from stable_baselines.common.noise import OrnsteinUhlenbeckActionNoise
from stable_baselines.ddpg import DDPG
import matplotlib.pyplot as plt
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import numpy as np

import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = "explicit", 'vae', 5
encoder_type = 'fixed'
random_seed = 50

env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type,
                  seed=random_seed, action_space_type='sb')

env.action_scaling = 0.06
env.plot_freq = 20
env.save_freq = 200
env.reward_fraction_sigma = 0.8
env.rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
env.reward_target = -0.3

env.max_episode_length = 20

total_timestamps, start_steps = 700, 0
act_noise = 0.03


hyp_para = {'rl_agent': 'DDPG_SB', 'total_timestamps': total_timestamps, 'start_steps': start_steps,
            'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma, 'act_noise':
                act_noise, 'target': env.reward_target, 'random_seed': random_seed}

env.save_settings(additional_log=hyp_para)

logger_kwargs = dict(output_dir='data/ddpg_' + env.now_str_all, exp_name='ddpd_am_' + env.now_str_all)

n_actions = env.action_space.shape[-1]
action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma=float(act_noise) * np.ones(n_actions))

model = DDPG(MlpPolicy, env, action_noise=action_noise, verbose=1, seed=random_seed, n_cpu_tf_sess=1,
             random_exploration=start_steps)

model.learn(total_timesteps=total_timestamps, log_interval=20)

#model.save("td3_awake")
# del model  # remove to demonstrate saving and loading
#model = TD3.load("td3_awake")

# path_plots = '/Users/fvelotti/Dropbox/Awake/Plots/ddpg_auto_matching/'
path_plots = '../data/'
env.fig.show()
env.fig.savefig(path_plots + hyp_para['rl_agent'] + '_actions_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig2.show()

env.fig2.savefig(path_plots + hyp_para['rl_agent'] +
                '_all_interactions_' + env.now_str_all + '.pdf')
plt.pause(1)

env.fig3.show()
env.fig3.savefig(path_plots + hyp_para['rl_agent'] + '_stats_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig5.show()

env.fig5.savefig(path_plots + hyp_para['rl_agent'] + '_states_' +
                env.now_str_all + '.pdf')
plt.pause(1)
