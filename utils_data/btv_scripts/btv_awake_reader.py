"""
Read HDF5 files saved from BTV for analysis
"""
import numpy as np
import matplotlib.pyplot as plt
import h5py
import glob
import os
"""
	Require madx-win64-gnu.exe and general_tt43.madx in same (madx) folder to 
	run madx to generate output files

	Expects inputs as (:BTV1_x,2_x,3_x,4_x,1_y,2_y,3_y,4_y)

	Input file locations for sequence/beam/element/etc files into madx file
	
	Uses linear least squares fit with the beam sizes and R matrix to 
	determine parameters dependent on the Twiss parameters and momentum spread
	
	Matrix equation for beamsize as:
	beamsize^2 = RMATRIX2.*q
	
	(sigma1^2)	(R11^2 2R11R12 R12^2 eta1^2)(beta(s0)epsilon)
	(sigma2^2)= (		....			 )(-alpha(s0)epsilon)
	(sigma3^2)	(		....			 )((1/beta(s0)(1+alpha^2)epsilon)
	(sigma4^2)	(		....			 )(deltap/p)
"""

# %% ---------------------------------------
path = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts'

madx_files_path = '/BTV/madx'
# %% ---------------------------------------
def BTV_calc(sigma_x, sigma_y, disp_y):
    #HORIZONTAL
    b = sigma_x
    #Load RMATRIX from file produced by MADX
    RMATRIX=np.loadtxt(path + madx_files_path + "/rmatrix.tfs", skiprows=8)[:-1, :]

    q = np.dot(np.linalg.inv(RMATRIX), b)
    
    #convertvoutput of fit (q) to Twiss parameters and momentum spread
    gamma_r = np.sqrt((18e-3)**2 + (0.511e-3)**2) / 0.511e-3
    epsilon_x=np.sqrt(np.abs((q[0]*q[2])-(q[1]*q[1]))) * gamma_r
    beta_x=q[0]/(epsilon_x)
    alpha_x=-q[1]/epsilon_x
    gamma_x=q[2]/epsilon_x

    # VERTICAL
    b = sigma_y
    eta= np.reshape(disp_y, (4,1))
    RMATRIX=np.loadtxt(path + madx_files_path + "/rmatrix_y.tfs", skiprows=8)
    #Add dispersion measurements to array
    RMATRIX2=np.append(RMATRIX,eta,axis=1)
    #Create array of beam size measurements
 
    q = np.dot(np.linalg.inv(RMATRIX2), b)

    #convert output of fit (q) to Twiss parameters and momentum spread
    epsilon_y=np.sqrt(np.abs((q[0]*q[2])-(q[1]*q[1]))) * gamma_r
    beta_y=q[0]/(epsilon_y)
    alpha_y=-q[1]/epsilon_y
    gamma_y=q[2]/epsilon_y
    momspr_y=np.sqrt(q[3])

    test=np.matmul(RMATRIX2,q)
    print('x : epsilon, beta, alpha')
    print(epsilon_x,beta_x,alpha_x)
    print('y : epsilon, beta, alpha, momentum spread')
    print(epsilon_y,beta_y,alpha_y, momspr_y)
# %% ---------------------------------------

def btv_meas(sig_x):
    rm = np.loadtxt(path + madx_files_path + "/rmatrix_new.tfs", skiprows=8)
    gamma_r = np.sqrt((18e-3)**2 + (0.511e-3)**2) / 0.511e-3
    c1 = rm[1, 0]
    s1 = rm[1, 1]
    c2 = rm[2, 0]
    s2 = rm[2, 1]
    w = ((sig_x[2] / sig_x[0])**2 / s2**2 - (sig_x[1]/sig_x[0])**2/s1**2 - (c2/s2)**2 + (c1/s1)**2) / ((c1/ s1) - (c2/s2))
    beta0 = 1 / (np.sqrt(np.abs((sig_x[2]/sig_x[0])**2/s2**2 - (c2/s2)**2 + w * (c2/s2) - w**2/4)))
    eps = sig_x[0]**2 / beta0 * gamma_r
    alpha0 = beta0/2 * w
    print(beta0, alpha0, eps)
# %% ---------------------------------------
path = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/'
folder_data = path + 'btv_meas_oct19/emit_meas_1/'

h5_files = glob.glob(folder_data + "/*.h5")

btvs = ['BTV42', 'TT43.BTV.430106', 'TT41.BTV.412350.LASER', 'BTV54']

if len(h5_files) != len(btvs):
    raise NameError('Number of files inconsistent with BTV names!')

# Switch between RMS or STD from Gaussian fit
rms_sigma = 'Sigma'

sigma_x = np.zeros(len(btvs))
sigma_y = np.zeros(len(btvs))
for i, data_file in enumerate(h5_files):
    f = h5py.File(data_file, 'r')
    if btvs[i] in data_file:
        sigma_x[i] = np.mean(f['x' + rms_sigma]) * 1e-3
        sigma_y[i] = np.mean(f['y' + rms_sigma]) * 1e-3
    f.close()
# sig_temp = sigma_x[0]
# sigma_x[0] = sigma_y[0]
# sigma_y[0] = sig_temp

# %% ---------------------------------------
disp_y = np.array([0.0, -0.217, 0.1385, -0.0038])
test_sig = np.array([0.00024, 0.00021, 0.00046])
# %%
BTV_calc(sigma_x[:-1]**2, sigma_y**2, np.array(disp_y))
# %%
btv_meas(sigma_x[:-1])
