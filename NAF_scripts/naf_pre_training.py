from awake_env.tl_opt_env_RL import tlOptEnv as environment
from pernaf.naf import NAF
from pernaf.utils.statistic import Statistic
import tensorflow as tf
import matplotlib.pyplot as plt
import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

folder_name = 'test_NAF_implicit_5d_test/'
directory = "checkpoints/" + folder_name

discount = 0.999
batch_size = 10
learning_rate = 2e-3
max_steps = 50
update_repeat = 7
max_episodes = 100 #50
tau = 1 - 0.999
nafnet_kwargs = dict(hidden_sizes=[256, 256, 256], activation=tf.nn.tanh
                     , weight_init=tf.random_uniform_initializer(-0.05, 0.05))
prio_info = dict(alpha=0.5, beta=.5)
noise_info = dict(noise_function=lambda nr: max(0, (1 - nr / 100)))

# Type of training
state_type, data_type, state_dof = "implicit", 'vae', 5
encoder_type = 'fixed'

env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof,
                  encoder_type=encoder_type, reward_dang=False, action_space_type='sb')
env.action_scaling = 0.2
env.rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]

env.reward_target = -0.45 # -0.25336
env.reward_dang_min = env.reward_target

env.reward_fraction_sigma = 0.8
env.plot_freq = 10
env.save_freq = 50

# Uses what's inside the folder and starts from there
is_train = True
is_continued = False

env.max_episode_length = 100

hyp_para = {'rl_agent': 'PERNAF', 'folder': folder_name, 'discount': discount, 'batch_size': batch_size,
            'learning_rate': learning_rate, 'max_steps': max_steps, 'update_rep': update_repeat,
            'max_episodes': max_episodes, 'tau': tau, 'is_train': is_train, 'is_continued': is_continued,
            'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma,
            'target': env.reward_target}

env.save_settings(additional_log=hyp_para)

with tf.Session() as sess:

    # statistics and running the agent
    stat = Statistic(sess=sess, env_name=env.__name__, model_dir=directory,
                     max_update_per_step=update_repeat, is_continued=is_continued, save_frequency=5)
    print('init the agent')
    agent = NAF(sess=sess, env=env, stat=stat, discount=
    discount, batch_size=batch_size,
                learning_rate=learning_rate, max_steps=max_steps, update_repeat=update_repeat,
                max_episodes=max_episodes, tau=tau, pretune = None, prio_info=prio_info, noise_info=noise_info,
                **nafnet_kwargs)

    init_op = tf.global_variables_initializer()  # create the graph
    sess.run(init_op)

    # if data_type == 'vae':
    #     print('loaded 1')
    #     env.model.model_btv._loadFiles()
    #
    # if state_type == 'implicit':
    #     print('loaded 2')
    #     env.stateEncoder._loadFiles()
    # run the agent
    agent.run(is_train)


path_plots = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/plots_1_12_19/'
env.fig.show()
env.fig.savefig(path_plots + hyp_para['rl_agent'] + '_actions_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig2.show()

env.fig2.savefig(path_plots + hyp_para['rl_agent'] +
                '_all_interactions_' + env.now_str_all + '.pdf')
plt.pause(1)

env.fig3.show()
env.fig3.savefig(path_plots + hyp_para['rl_agent'] + '_stats_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig5.show()

env.fig5.savefig(path_plots + hyp_para['rl_agent'] + '_states_' +
                env.now_str_all + '.pdf')
plt.pause(1)

# new_target = env.reward_target * 1.05
#
# del env
# del agent
# del stat
#
# #===================================================
# # Training of saturated reward
# #===================================================
# tf.keras.backend.clear_session()
#
# env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof,
#                   encoder_type=encoder_type)
# env.action_scaling = 0.2
# env.rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
#
# env.reward_target = new_target
# print('New target set to: ', env.reward_target)
# max_episodes = 200
#
# env.reward_fraction_sigma = 0.7
# env.plot_freq = 1
# env.save_freq = 50
#
# hyp_para = {'rl_agent': 'PERNAF', 'folder': folder_name, 'discount': discount, 'batch_size': batch_size,
#             'learning_rate': learning_rate, 'max_steps': max_steps, 'update_rep': update_repeat,
#             'max_episodes': max_episodes, 'tau': tau, 'is_train': is_train, 'is_continued': is_continued,
#             'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma,
#             'target': env.reward_target}
#
# env.save_settings(additional_log=hyp_para)
#
# with tf.Session() as sess:
#
#     # statistics and running the agent
#     stat = Statistic(sess=sess, env_name=env.__name__, model_dir=directory,
#                      max_update_per_step=update_repeat, is_continued=is_continued, save_frequency=5)
#     print('init the agent')
#     agent = NAF(sess=sess, env=env, stat=stat, discount=
#     discount, batch_size=batch_size,
#                 learning_rate=learning_rate, max_steps=max_steps, update_repeat=update_repeat,
#                 max_episodes=max_episodes, tau=tau, pretune = None, prio_info=prio_info, noise_info=noise_info,
#                 **nafnet_kwargs)
#
#     init_op = tf.global_variables_initializer()  # create the graph
#     sess.run(init_op)
#
#     # run the agent
#     agent.run(is_train)
# path_plots = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/plots_1_12_19/'
# env.fig.show()
# env.fig.savefig(path_plots + hyp_para['rl_agent'] + '_actions_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig2.show()
#
# env.fig2.savefig(path_plots + hyp_para['rl_agent'] +
#                 '_all_interactions_' + env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig3.show()
# env.fig3.savefig(path_plots + hyp_para['rl_agent'] + '_stats_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig5.show()
#
# env.fig5.savefig(path_plots + hyp_para['rl_agent'] + '_states_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
#
# del env
# del agent
# del stat
#
# #===================================================
# # Validation
# #===================================================
# tf.keras.backend.clear_session()
#
# env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof,
#                   encoder_type=encoder_type)
# env.action_scaling = 0.2
# env.rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
#
# env.reward_target = new_target
# print('New target set to: ', env.reward_target)
# max_episodes = 50
#
# env.reward_fraction_sigma = 0.7
# env.plot_freq = 1
# env.save_freq = 50
#
# is_train = False
# is_continued = True
#
# hyp_para = {'rl_agent': 'PERNAF', 'folder': folder_name, 'discount': discount, 'batch_size': batch_size,
#             'learning_rate': learning_rate, 'max_steps': max_steps, 'update_rep': update_repeat,
#             'max_episodes': max_episodes, 'tau': tau, 'is_train': is_train, 'is_continued': is_continued,
#             'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma,
#             'target': env.reward_target}
#
# env.save_settings(additional_log=hyp_para)
#
# with tf.Session() as sess:
#
#     # statistics and running the agent
#     stat = Statistic(sess=sess, env_name=env.__name__, model_dir=directory,
#                      max_update_per_step=update_repeat, is_continued=is_continued, save_frequency=5)
#     print('init the agent')
#     agent = NAF(sess=sess, env=env, stat=stat, discount=
#     discount, batch_size=batch_size,
#                 learning_rate=learning_rate, max_steps=max_steps, update_repeat=update_repeat,
#                 max_episodes=max_episodes, tau=tau, pretune = None, prio_info=prio_info, noise_info=noise_info,
#                 **nafnet_kwargs)
#
#     init_op = tf.global_variables_initializer()  # create the graph
#     sess.run(init_op)
#
#     # run the agent
#     agent.run(is_train)
#
# path_plots = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/plots_1_12_19/'
# env.fig.show()
# env.fig.savefig(path_plots + hyp_para['rl_agent'] + '_actions_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig2.show()
#
# env.fig2.savefig(path_plots + hyp_para['rl_agent'] +
#                 '_all_interactions_' + env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig3.show()
# env.fig3.savefig(path_plots + hyp_para['rl_agent'] + '_stats_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
#
# env.fig5.show()
#
# env.fig5.savefig(path_plots + hyp_para['rl_agent'] + '_states_' +
#                 env.now_str_all + '.pdf')
# plt.pause(1)
