import numpy as np
import pickle
import os
from trained_model import trainedmodel

import tensorflow as tf

import time

t0 = time.time()

tf_version = int(tf.__version__.split('.')[0])
if tf_version < 2:
    from keras.models import load_model
else:
    print('Running with TF2 ++++++++++++++++++++++++++++++++++++++++')
    from tensorflow import keras
    load_model = keras.models.load_model


import pandas as pd
from stable_baselines import TD3
from stable_baselines.td3.policies import MlpPolicy
from stable_baselines.ddpg.noise import NormalActionNoise
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import datetime
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

state_type, data_type, state_dof = "implicit", "vae", 7

encoder_type = "fixed"
random_seed = 0

action_scaling = 0.1
plot_freq = 2
no_plot = True
save_freq = 3
reward_fraction_sigma = 0.8
rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
max_episode_length = 100

reward_target_start = -0.5
learning_starts = 100

x0_scaling = 0.8

model_path = "trained_model/new_models/"
# enc_path = model_path + 'encoder_AWAKE_multiple_VAE_Fake_1.hdf5'
# enc_path = model_path + "encoder_AWAKE_VAE_all_data_128D.hdf5"
# dec_path = model_path + "decoder_AWAKE_VAE_all_data_128D.hdf5"
# zpr_path = model_path + "zpredictor_AWAKE_VAE_all_data_128D.hdf5"

now = (
    str(datetime.datetime.now())
    .replace(" ", "_")
    .replace(":", "_")
    .split(".")[0]
)
logdir = f"data/{now}_{data_type}_5d/"
os.mkdir(logdir)

# %% ===================================


class NewModel(trainedmodel.TrainedModel):
    def _loadFiles(self):
        # self.encoderModel = load_model(enc_path, compile=False)
        # self.decoderModel = load_model(dec_path, compile=False)
        # self.zpredictorModel = load_model(zpr_path, compile=False)
        self.is_log = True
        self.pixel_norm = 3604

        self.encoderModel = load_model(
            self.path + "encoder" + self.fn, compile=False
        )
        self.decoderModel = load_model(
            self.path + "decoder" + self.fn, compile=False
        )
        self.zpredictorModel = load_model(
            self.path + "zpredictor" + self.fn, compile=False
        )


new_model = {"model": NewModel(None, state_dof)}
new_model = {}
log_path = {"path": logdir}

print("initializing new model")
env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
    reward_dang=True,
    no_plot=no_plot,
    log_path=log_path,
    new_model=new_model,
)

# logdir = f'data/{env.now_str_all.split(".")[0]}_{data_type}'


env.set_log_path({"path": logdir})

n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(
    mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions)
)

env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0
env.x0_scaling = x0_scaling
env.set_x0()

env.max_episode_length = max_episode_length


env.reward_target = reward_target_start
env.reward_dang_min = env.reward_target

total_timesteps = 600

hyp_params = {
    "rl_agent": "SB TD3",
    "learning_starts": learning_starts,
    "total_timesteps": total_timesteps,
    "action_scaling": env.action_scaling,
    "reward_fraction_sigma": env.reward_fraction_sigma,
    "action_noise": action_noise,
    "reward_target": env.reward_target,
    "random_seed": random_seed,
}

env.save_settings(additional_log=hyp_params)

model = TD3(
    MlpPolicy,
    env,
    action_noise=action_noise,
    verbose=1,
    learning_starts=learning_starts,
    seed=random_seed,
    n_cpu_tf_sess=1,
)
model.learn(total_timesteps=total_timesteps, log_interval=20)

# # env.update_all_plots()

# # env.fig.show()
# # plt.pause(1)
# env.fig.savefig(logdir + "/actions" + env.now_str_all + ".png")

# # env.fig2.show()
# # plt.pause(1)
# env.fig2.savefig(logdir + "/all_interactions_" + env.now_str_all + ".png")

# # env.fig3.show()
# # plt.pause(1)
# env.fig3.savefig(logdir + "/stats_" + env.now_str_all + ".png")

# # env.fig5.show()
# # plt.pause(1)
# env.fig5.savefig(logdir + "/states_" + env.now_str_all + ".png")


print("Making first plot")

fig_final, axis = plt.subplots(nrows=1, ncols=3, figsize=(8, 2.5))
axis[0].plot(
    np.array(env.reward_list),
    "o",
    color="k",
    label=r"r$_{tot}$",
    alpha=0.25,
)
axis[0].plot(env.reward_i, ".", c="b", label="r$_a$", alpha=0.3)
axis[0].plot(
    env.reward_xy,
    "r.",
    label=r"r$_{\sigma}$",
    alpha=0.3,
)
axis[0].set_ylim(-1.0, 0.2)
axis[0].plot(env.target_list, ls="--")
axis[0].set_xlabel("Iterations")
axis[0].set_ylabel("Reward")
axis[0].legend(fontsize=7)

axis[2].plot(env.reward_per_episode["disc"], label="1 step")
axis[2].set_ylabel("r$_{ep}$")
axis[2].set_xlabel("Episode #")
# ==============================================
# Re-training with new reward
# ==============================================

new_target = env.reward_target * 1.05

del env

env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
    no_plot=no_plot,
    log_path=log_path,
    new_model=new_model,
)

n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(
    mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions)
)

env.set_log_path({"path": logdir})
env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0

env.x0_scaling = x0_scaling
env.set_x0()

env.reward_target = new_target
env.max_episode_length = max_episode_length

total_timesteps = 500

hyp_params = {
    "rl_agent": "SB TD3",
    "learning_starts": learning_starts,
    "total_timesteps": total_timesteps,
    "action_scaling": env.action_scaling,
    "reward_fraction_sigma": env.reward_fraction_sigma,
    "action_noise": action_noise,
    "reward_target": env.reward_target,
    "random_seed": random_seed,
}

env.save_settings(additional_log=hyp_params)

model = TD3(
    MlpPolicy,
    env,
    action_noise=action_noise,
    verbose=1,
    learning_starts=learning_starts,
    seed=random_seed,
    n_cpu_tf_sess=1,
)
model.learn(total_timesteps=total_timesteps, log_interval=20)

# env.update_all_plots()
# env.fig.show()
# plt.pause(1)
# env.fig.savefig(logdir + "/actions" + env.now_str_all + ".png")

# # env.fig2.show()
# # plt.pause(1)
# env.fig2.savefig(logdir + "/all_interactions_" + env.now_str_all + ".png")

# # env.fig3.show()
# # plt.pause(1)
# env.fig3.savefig(logdir + "/stats_" + env.now_str_all + ".png")

# # env.fig5.show()
# # plt.pause(1)
# env.fig5.savefig(logdir + "/states_" + env.now_str_all + ".png")

model.save(logdir + "/model.h5")

axis[1].plot(
    np.array(env.reward_list),
    "o",
    color="k",
    label=r"r$_{tot}$",
    alpha=0.25,
)
axis[1].plot(env.reward_i, ".", c="b", label="r$_a$", alpha=0.3)
axis[1].plot(
    env.reward_xy,
    "r.",
    label=r"r$_{\sigma}$",
    alpha=0.3,
)
axis[1].set_ylim(-1.0, 0.2)
axis[1].plot(env.target_list, ls="--")
axis[1].set_xlabel("Iterations")
axis[1].set_ylabel("Reward")
axis[1].legend(fontsize=7)

axis[2].plot(env.reward_per_episode["disc"], label="2 step")
axis[2].legend(fontsize=7)
fig_final.tight_layout()
fig_final.savefig(logdir + "/plot_reward_dang_td3.pdf")


# =================================================
# Validation
# =================================================
del env

env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
    no_plot=no_plot,
    log_path=log_path,
    new_model=new_model,
)

n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(
    mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions)
)

env.set_log_path({"path": logdir})
env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0

env.x0_scaling = x0_scaling
env.set_x0()

env.reward_target = new_target

# Load saved model
print("Load model")
model = model.load(logdir + "/model.h5", env=env)

obs = env.reset()

# Evaluate the agent
tot_reward_val = []
tot_len = []
final_action = []
episode_reward = 0
count = 1
success = False
for _ in range(100):
    action, _ = model.predict(obs)
    obs, reward, done, info = env.step(action)
    # env._render_per_interation()
    episode_reward += reward
    count += 1
    if reward > env.reward_target:
        success = True
        episode_reward += 10
        tot_reward_val.append(episode_reward)
        tot_len.append(count)
        final_action.append(env.get_actions())

        count = 1
        episode_reward = 0.0
    if done:
        print("Reward:", episode_reward, "Success?", success)
        success = False
        obs = env.reset()

pickle.dump(
    (tot_reward_val, tot_len, env.reward_target),
    open(logdir + "/train_result.p", "wb"),
)

# env.update_all_plots()
# # env.fig.show()
# # plt.pause(1)
# env.fig.savefig(logdir + "/actions" + env.now_str_all + ".png")

# # env.fig2.show()
# # plt.pause(1)
# env.fig2.savefig(logdir + "/all_interactions_" + env.now_str_all + ".png")

# # env.fig3.show()
# # plt.pause(1)
# env.fig3.savefig(logdir + "/stats_" + env.now_str_all + ".png")

# # env.fig5.show()
# # plt.pause(1)
# env.fig5.savefig(logdir + "/states_" + env.now_str_all + ".png")
print("time needed:")
print(time.time() - t0)
