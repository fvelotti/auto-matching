import pyjapc
import pickle
import os

japc = pyjapc.PyJapc('SPS.USER.ALL')
japc.rbacLogin()

devices = ['RPSKN.TSG4.SNH.430000',
                        'RPSKN.TSG4.SNJ.430001',
                        # 'RPCAH.TSG4.RCIBH.430011',
                        # 'RPCAH.TSG4.RCIBV.430011',
                        # 'RPCAH.TSG4.RCIBH.412353',
                        # 'RPCAH.TSG4.RCIBV.412353',
                        'logical.RQID.430031',
                        'logical.RQIF.430034',
                        'logical.RQID.430037']


def set_current(device, value):
    japc.setParam(device + '/SettingPPM#current', value, timingSelectorOverride='')
    print(device + ' set to: ' + str(value))


def get_current(device):
    value = japc.getParam(device + '/SettingPPM#current', timingSelectorOverride='')
    return value


def get_K(device):
    value = japc.getParam(device + '/K', timingSelectorOverride='')
    return value

def set_K(device, value):
        japc.setParam(device + '/K', value, timingSelectorOverride='')
        print(device + ' set to: ' + str(value))


ini_file_exists = os.path.isfile('../awake_env/initial_para_last.pkl')

if not ini_file_exists:
    ini_values = []
    for i, device in enumerate(devices):

        print('Getting ' + device)
        if 'logical' in device:
            value = get_K(device)
        else:
            value = get_current(device)
        ini_values.append(value)

    dic = {'devices': devices, 'values': ini_values}
    pickle.dump(dic, open('../awake_env/initial_para_last.pkl', 'wb'))

else:
    test_user = input('File found - do you want to set? (True or False)\n')
    if test_user == 'True':
        print(test_user)
        ini_file = pickle.load(open('../awake_env/initial_para_last.pkl', 'rb'))
        for i, device in enumerate(devices):
            if 'logical' in device:
                set_K(device, ini_file['values'][i])
            else:
                set_current(device, ini_file['values'][i])
