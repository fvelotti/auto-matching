from matplotlib import pyplot as plt
import numpy as np
import pickle
import os
from keras.models import load_model
from trained_model import trainedmodel

import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.optimizers import Adam
import tensorflow_probability as tfp

import pandas as pd
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import datetime
import h5py

from ilqr import iLQR
from ilqr.dynamics import FiniteDiffDynamics
from ilqr.cost import QRCost
from ilqr.cost import FiniteDiffCost

# tf.compat.v1.disable_v2_behavior()

# tf_config = tf.ConfigProto()
# tf_config.gpu_options.allow_growth = True
# sess = tf.Session(config=tf_config)

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

# for bayesian stuff
tfd = tfp.distributions
tfb = tfp.bijectors
tf_type = tf.float64

def neg_log_likelihood(y, dist):
    return -dist.log_prob(y)


def get_bayesian_model(dim, d_type):  # Nico
    # input
    input_x = tf.keras.Input(shape=(dim[0], ), dtype=d_type)
    input_u = tf.keras.Input(shape=(dim[1], ), dtype=d_type)
    concl = layers.concatenate([input_x, input_u], dtype=d_type)
    # hidden layers
    hl = layers.Dense(
        15,
        activation='relu',
        dtype=d_type,
        kernel_regularizer=tf.keras.regularizers.l1(1e-6),
        bias_regularizer=tf.keras.regularizers.l1(1e-6),
    )(concl)
    # hl = layers.Dropout(0.1, dtype=d_type)(hl)
    hl = layers.Dense(
        15,
        activation='relu',
        dtype=d_type,
        kernel_regularizer=tf.keras.regularizers.l1(1e-6),
        bias_regularizer=tf.keras.regularizers.l1(1e-6),
    )(hl)
    out_l = layers.Dense(dim[0], dtype=d_type)(hl)
    mod = tf.keras.Model(inputs=[input_x, input_u], outputs=out_l)
    return mod

def make_r_model(dim_s, dim_r, d_type):
    input_x = tf.keras.Input(shape=(dim_s,), dtype=d_type)
    # hidden layers
    hl = layers.Dense(
        15,
        activation='relu',
        dtype=d_type,
        kernel_regularizer=tf.keras.regularizers.l1(1e-6),
        bias_regularizer=tf.keras.regularizers.l1(1e-6),
    )(input_x)
    # hl = layers.Dropout(0.1, dtype=d_type)(hl)
    hl = layers.Dense(
        15,
        activation='relu',
        dtype=d_type,
        kernel_regularizer=tf.keras.regularizers.l1(1e-6),
        bias_regularizer=tf.keras.regularizers.l1(1e-6),
    )(hl)

    out_l = layers.Dense(dim_r, dtype=d_type, activation='linear')(hl)
    mod = tf.keras.Model(inputs=input_x, outputs=out_l)
    return mod


state_type, data_type, state_dof = 'implicit', 'btv', 5

encoder_type = 'fixed'
random_seed = 0

btv = 'btv53'

btvs = {'btv53': 'TT41.BTV.412353.LASER', 'btv54': 'BTV54'}

btv_name = btvs[btv]

action_scaling = 0.4
plot_freq = 1
no_plot = False
save_freq = 2
reward_fraction_sigma = 0.6
rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
max_episode_length = 100

reward_target_start = -0.2
learning_starts = 100

x0_scaling = 0.8

btv_scaling = 1.3e6 * 2
r_max = np.sqrt(1 ** 2 + 1 ** 2)

model_path = 'trained_model/new_models/'
# enc_path = model_path + 'encoder_AWAKE_multiple_VAE_Fake_1.hdf5'
enc_path = model_path + 'encoder_AWAKE_VAE_all_data_128D.hdf5'
dec_path = model_path + 'decoder_AWAKE_VAE_all_data_128D.hdf5'
zpr_path = model_path + 'zpredictor_AWAKE_VAE_all_data_128D.hdf5'

now = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_').split(".")[0]
logdir = f'data/{now}_{data_type}_{state_dof}d_{btv}/'
os.mkdir(logdir)

class NewModel(trainedmodel.TrainedModel):
    def _loadFiles(self):
        self.encoderModel = load_model(enc_path, compile=False)
        self.decoderModel = load_model(dec_path, compile=False)
        self.zpredictorModel = load_model(zpr_path, compile=False)
        self.is_log = True
        self.pixel_norm = 3604

        # self.encoderModel = load_model(self.path + 'encoder' + self.fn, compile=False)
        # self.decoderModel = load_model(self.path + 'decoder' + self.fn, compile=False)
        # self.zpredictorModel = load_model(self.path + 'zpredictor' + self.fn, compile=False)


# new_model = {'model': NewModel(None, state_dof)}
new_model = {}
log_path = {'path': logdir}

# neural network for state-delta action mapping:
mf = get_bayesian_model([state_dof, 5], tf_type)

model_state_rew = make_r_model(state_dof, 1, tf_type)

env = environment(None, state_type=state_type,
                  data_type=data_type, state_dof=state_dof, seed=random_seed,
                  encoder_type=encoder_type, action_space_type='sb', reward_dang=True,
                  no_plot=no_plot, log_path=log_path, new_model=new_model, btv_name=btv_name)

env.set_log_path({'path': logdir})

n_actions = env.action_space.shape[-1]

env.i0 = btv_scaling
env.r_max = r_max

env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0
env.x0_scaling = x0_scaling
env.set_x0()

env.max_episode_length = max_episode_length

env.reward_target = reward_target_start
env.reward_dang_min = env.reward_target

hyp_params = {'rl_agent': 'action_scan',
              'action_scaling': env.action_scaling,
              'reward_fraction_sigma': env.reward_fraction_sigma,
              'reward_target': env.reward_target,
              'random_seed': random_seed,
              'btv_scaling': btv_scaling,
              'btv': btv_name,
              'r_max': r_max
              }

env.save_settings(additional_log=hyp_params)

def get_data(env, n_data, d_type):

    x_list, u_list, y_list, r_list = [], [], [], []
    for _ in range(n_data):
        s0 = env.reset()
        x_val = tf.convert_to_tensor(s0, d_type)
        a = env.action_space.sample()
        u_val = tf.convert_to_tensor(a, d_type)

        s1, r1 = env.step(a)[0:2]
        r_val = tf.convert_to_tensor(r1, dtype=tf.float64)
        y_val = tf.convert_to_tensor(s1, dtype=tf.float64)

        x_list.append(x_val)
        u_list.append(u_val)
        y_list.append(y_val)
        r_list.append(r_val)

    return tf.stack(x_list), tf.stack(u_list), tf.stack(y_list), tf.stack(r_list)

def get_filename():

    system = 'AWAKE_'

    # current date and time
    now = datetime.datetime.now()
    date = now.strftime("%Y_%m_%d_%H_%M_%S")

    model = "_model"

    hdf5_ext = '.hdf5'
    h5_ext = '.h5'

    filename = [system + date + hdf5_ext, system + date + model + h5_ext]

    return filename


def check_filename(filename):
    if os.path.isfile(logdir + filename):
        os.remove(logdir + filename)


def store(filename, group, dataset_list, data_list):
    address = logdir + filename
    if os.path.isfile(address):
        hdf = h5py.File(address, 'a')
    else:
        hdf = h5py.File(address, 'w')
    print(data_list)
    g = hdf.create_group(group)
    for idx in range(len(dataset_list)):
        print(idx)
        g.create_dataset(dataset_list[idx], data=data_list[idx])

    hdf.close()
    return 0

# Create HDF5 storage file
fname, mname = get_filename()
check_filename(fname)
check_filename(mname)

x_dim = env.observation_space.shape[0]
u_dim = env.action_space.shape[0]

# Max action allowed
u_max = env.action_space.high[0]

trainIt = True

get_data_machine = True

file_name_data = 'data/2020-08-11_16_04_58_btv_5d/data_awake_ilqr.p'
if trainIt:
    num_data = 600  # 150  # 250  # 500
    th_data = np.int(np.around(0.75 * num_data))

    # get data to train the neural network:
    if get_data_machine:
        x_set, u_set, y_set, r_set = get_data(env, num_data, tf_type)
        store(filename=fname,
              group='data',
              dataset_list=['n_data', 'train%', 'x_set', 'u_set', 'y_set', 'r_set'],
              data_list=[num_data, th_data, x_set, u_set, y_set, r_set])

    else:
        x_set, u_set, y_set, r_set = pickle.load(open(file_name_data, 'rb'))

    # compile system dynamics model:

    mf.compile(optimizer=Adam(0.001), loss='mse')

    model_state_rew.compile(optimizer=Adam(0.001), loss='mse')
    print(mf.summary())
    print(model_state_rew.summary())

    checkpoint_path = 'data/' + mname[:-3]+'.ckpt'
    # EarlyStopping callback
    callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', patience=5)  # patience=5

    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                        save_weights_only=True,
                                                        verbose=1)

    history = mf.fit(x=(x_set, u_set),
                     y=y_set[:th_data, :],
                     validation_split=0.3,
                     epochs=200,
                     callbacks=[callback, cp_callback],
                     verbose=1)

    print('Saving model...')
    mf.save(logdir + mname)

    history_r = model_state_rew.fit(x=y_set,
                                    y=r_set,
                                    epochs=200,
                                    callbacks=[callback, cp_callback],
                                    validation_split=0.3,
                                    verbose=1)

    print('Saving model...')
    model_state_rew.save(logdir + 'state_rew_model.h5')

    #store(filename=fname,
    #      group='training_history',
    #      dataset_list=['history'],
    #      data_list=[history])

    plt.figure()
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('# epoch')
    plt.ylabel('error')

    plt.legend()

    plt.figure()
    plt.plot(history_r.history['val_loss'], label='val_loss')
    plt.plot(history_r.history['loss'], label='loss')
    plt.xlabel('# epoch')
    plt.ylabel('error')

    plt.legend()

else:

    # with h5py.File('data/2020-08-11_14_51_25_vae_7d/AWAKE_2020_08_11_14_51_26.hdf5') as
    mf = tf.keras.models.load_model('data/2020-08-11_14_51_25_vae_7d/AWAKE_2020_08_11_14_51_26_model.h5',
                                    compile=False)
    model_state_rew = tf.keras.models.load_model('data/2020-08-11_14_51_25_vae_7d/state_rew_model.h5',
                                                 compile=False)

# end training

# get data for test
x = tf.expand_dims(tf.convert_to_tensor(env.reset(), dtype=tf_type), axis=0)
u = tf.expand_dims(tf.convert_to_tensor(env.action_space.sample(), dtype=tf_type), axis=0)


def func(x, u, i):
    """Dynamics model function.

    Args:
        x: State vector [state_size].
        u: Control vector [action_size].
        i: Current time step.

    Returns:
        Next state vector [state_size].
    """
    x = tf.expand_dims(tf.convert_to_tensor(x, dtype=tf.float64),  axis=0)
    u = tf.expand_dims(tf.convert_to_tensor(u, dtype=tf.float64), axis=0)

    return np.squeeze(mf([x, u]))


def l(x, u, i):
    """Instantaneous cost function.

    Args:
        x: State vector [state_size].
        u: Control vector [action_size].
        i: Current time step.

    Returns:
        Instantaneous cost [scalar].
    """
    x_inp = tf.expand_dims(tf.convert_to_tensor(x, dtype=tf.float64),  axis=0)
    y = -1 * np.squeeze(model_state_rew(x_inp))
    return (y * Q * y) + (u.T @ R @ u)


def l_terminal(x, i):
    """Terminal cost function.

    Args:
        x: State vector [state_size].
        i: Current time step.

    Returns:
        Terminal cost [scalar].
    """
    x_inp = tf.expand_dims(tf.convert_to_tensor(x, dtype=tf.float64), axis=0)
    y = -1 * np.squeeze(model_state_rew(x_inp))
    return y * Q * y


state_size = env.observation_space.shape[0]
action_size = env.action_space.shape[0]

# NOTE: Unlike with AutoDiffDynamics, this is instantaneous, but will not be as accurate.
dynamics = FiniteDiffDynamics(func, state_size, action_size)

# Q, R used in l and l_terminal but they are not declared inside the methods
Q = 1e3 * np.eye(1)  # Q = 1000 * np.eye(state_size)
R = 1e0 * np.eye(action_size)  # R = 0.01 * np.eye(action_size)

# # NOTE: Unlike with AutoDiffCost, this is instantaneous, but will not be as accurate.
cost = FiniteDiffCost(l, l_terminal, state_size, action_size)
#
N = 3  # Number of time-steps in trajectory. (Prediction time)
N_ep = 10  # 50
max_nstep = 10  # 20  # 10

store(filename=fname,
      group='iLQR_setup',
      dataset_list=['pred_time', 'n_episode', 'max_n_step', 'Q', 'R'],
      data_list=[N, N_ep, max_nstep, Q, R])


ilqr = iLQR(dynamics, cost, N)

th = env.reward_target

for ep in range(N_ep):
    n_ep = "EPISODE #"+str(ep+1)
    print('\n', n_ep, '\n')

    # Initial state ---> the good one!
    check = True
    
    while check:
        x0 = env.reset()            # Initial state.
        r0 = env.reward  # getting reward from env after reset
        print(" r0 in prep loop ", r0)
        #r0 = env._get_reward(x0)    # Initial reward
        if r0 < th: #th
            check = False

    x_first = x0
    print('Initial state: ', x0)
    print('Initial reqrd: ', r0)

    # initialize iLQR

    # Action sequence was initially set random.randn now it is set to zeros. Both sets work!
    # us_init = np.array([np.random.randn(action_size)*env.action_scale for _ in range(N)])
    us_init = np.array([np.zeros(action_size) * env.action_scaling for _ in range(N)])
    us = us_init

    x_end = []
    x_end.append(x0)

    # storage lists:
    dataset_list = ['state', 'action', 'reward', 'success', 'n_step', 'th']
    x_list, u_list, r_list = [], [], []
    cnt_list, th_list = [], []
    x_list.append(x0), u_list.append(us[0]), r_list.append(r0), th_list.append(env.reward_target)

    cnt = 0
    cnt_list.append(cnt)

    run_cond = True
    while run_cond:
        cnt = cnt + 1
        print('Loop iteration # ', cnt)
        t_init = datetime.datetime.now()
        # ilqr.fit() takes very long time, ~130[s] with N = 5, ~20[s] with N = 2, ~8[s] with N = 1
        #us = np.array([np.zeros(action_size) * env.action_scale for _ in range(N)])
        xs, us = ilqr.fit(x0, us, n_iterations=N)
        t_now = datetime.datetime.now()
        fit_time = t_now - t_init

        # clip us[0] with respect to u_max
        us[0] = np.clip(us[0], -u_max, u_max)

        ## x0 = env._calculate_trajectory_iLQR(x0, us[0], env.plane)
        x0, r1, _, _ = env.step(us[0]) # this is it!!!!!

        print('last action: ', us[0])
        # r1 = env._get_reward(x0)
        u_list.append(us[0])
        r_list.append(-r1)
        x_list.append(x0)
        cnt_list.append(cnt)
        th_list.append(-th)

        # in AWAKE_simulated_env il reward è definito: rms = np.sqrt(np.mean(np.square(trajectory)))
        # x_end.append(np.sum(np.square(x0)))
        x_end.append(r1)

        r = r1  
        print('current reward: ', r)
        
        t_fin = datetime.datetime.now()
        dt = t_fin - t_init

        print("threshold ", th)
        print('Duration of the iteration:', dt.total_seconds(), '[s]')
        
        if r > th:
            print('\nTARGET REACHED!\n')
            store(filename=fname,
                  group=n_ep,
                  dataset_list=dataset_list,
                  data_list=[x_list, u_list, r_list, True, cnt, th])
            run_cond = False  # run_cond = False
        elif cnt == max_nstep:  # 5:
            print('\nOptimization FAILED!\n')
          
            run_cond = False  # run_cond = False
        else:
            print('\nAnother step is required...\n')

    print('Final state: ', x0)
    x_last = x0

