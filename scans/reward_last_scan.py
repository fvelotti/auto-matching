import pandas as pd
from stable_baselines import TD3
from stable_baselines.td3.policies import MlpPolicy
from stable_baselines.ddpg.noise import NormalActionNoise
from awake_env.tl_opt_env_RL import tlOptEnv as environment

from matplotlib import pyplot as plt
import numpy as np
import pickle

import datetime
import shutil
import os
import tensorflow as tf

import seaborn as sns

sns.set_style('white')

tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = 'implicit', 'vae', 5
encoder_type = 'fixed'
random_seed = 0

action_scaling = 0.1
plot_freq = 50
save_freq = 50
reward_fraction_sigma = 0.8
rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
max_episode_length = 100

discount = 0.99

reward_target = -0.45

learning_starts = 100

interactions_first_train = 600
interactions_second_train = 300
interactions_validation = 100

scan_name = 'reward_frac'

rew_frac = np.linspace(1.4, 1.02, 20, endpoint=True)

# %% ===================================

now = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_').split('.')[0]
base_dir = f'data/scan_{scan_name}_{now}/'

tot_len_mean_all = np.zeros(len(rew_frac))
tot_len_std_all = np.zeros(len(rew_frac))

tot_rew_mean_all = np.zeros(len(rew_frac))
tot_rew_std_all = np.zeros(len(rew_frac))

tot_target_all = np.zeros(len(rew_frac))

for i, rew_frac_i in enumerate(rew_frac):
    fold_name = f'{scan_name}_{i}'
    logdir = base_dir + fold_name
    shutil.rmtree(logdir, ignore_errors=True)
    os.makedirs(logdir)

    try:
        del env
    except:
        print('env not there yet.')

    log_path = {'path': logdir}

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb',
                      reward_dang=True, no_plot=True,
                      log_path=log_path)

    n_actions = env.action_space.shape[-1]
    action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

    env.action_scaling = action_scaling
    env.plot_freq = plot_freq
    env.save_freq = save_freq
    env.reward_fraction_sigma = reward_fraction_sigma
    env.rand_x0 = rand_x0
    env.max_episode_length = max_episode_length

    env.gamma = discount

    env.reward_target = reward_target
    env.reward_dang_min = env.reward_target

    total_timesteps = interactions_first_train

    hyp_params = {'rl_agent': 'SB TD3',
                  'learning_starts': learning_starts,
                  'total_timesteps': total_timesteps,
                  'action_scaling': env.action_scaling,
                  'reward_fraction_sigma': env.reward_fraction_sigma,
                  'action_noise': action_noise,
                  'reward_target': env.reward_target,
                  'random_seed': random_seed,
                  'gamma': discount
                  }

    env.save_settings(additional_log=hyp_params)

    model = TD3(MlpPolicy, env, action_noise=action_noise, verbose=1, learning_starts=learning_starts,
                seed=random_seed, n_cpu_tf_sess=1, gamma=discount)
    model.learn(total_timesteps=total_timesteps, log_interval=20)

    env.update_all_plots()
    # env.fig.show()
    # plt.pause(1)
    env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

    # env.fig2.show()
    # plt.pause(1)
    env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

    # env.fig3.show()
    # plt.pause(1)
    env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

    # env.fig5.show()
    # plt.pause(1)
    env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

    # ==============================================
    # Re-training with new reward
    # ==============================================

    new_target = env.reward_target * rew_frac_i

    del env

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb',
                      no_plot=True, log_path=log_path)

    n_actions = env.action_space.shape[-1]

    env.action_scaling = action_scaling
    env.plot_freq = plot_freq
    env.save_freq = save_freq
    env.reward_fraction_sigma = reward_fraction_sigma
    env.rand_x0 = rand_x0
    env.reward_target = new_target
    env.max_episode_length = max_episode_length

    total_timesteps = interactions_second_train

    hyp_params = {'rl_agent': 'SB TD3', 'learning_starts': learning_starts, 'total_timesteps': total_timesteps,
                  'action_scaling': env.action_scaling, 'reward_fraction_sigma': env.reward_fraction_sigma,
                  'action_noise': action_noise, 'reward_target': env.reward_target, 'random_seed': random_seed,
                  }

    env.save_settings(additional_log=hyp_params)

    model = TD3(MlpPolicy, env, action_noise=action_noise, verbose=1, learning_starts=learning_starts,
                seed=random_seed, n_cpu_tf_sess=1)
    model.learn(total_timesteps=total_timesteps, log_interval=20)

    env.update_all_plots()
    # env.fig.show()
    # plt.pause(1)
    env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

    # env.fig2.show()
    # plt.pause(1)
    env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

    # env.fig3.show()
    # plt.pause(1)
    env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

    # env.fig5.show()
    # plt.pause(1)
    env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

    model.save(logdir + '/model.h5')

    # =================================================
    # Validation
    # =================================================
    del env

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb',
                      no_plot=True, log_path=log_path)

    env.action_scaling = action_scaling
    env.plot_freq = plot_freq
    env.save_freq = save_freq
    env.reward_fraction_sigma = reward_fraction_sigma
    env.rand_x0 = rand_x0
    env.reward_target = new_target

    # Load saved model
    print('Load model')
    model = model.load(logdir + '/model.h5', env=env)

    obs = env.reset()

    # Evaluate the agent
    tot_reward_val = []
    tot_len = []
    episode_reward = 0
    count = 1
    success = False
    for _ in range(interactions_validation):
        action, _ = model.predict(obs)
        obs, reward, done, info = env.step(action)
        env._render_per_interation()
        episode_reward += reward * discount**(count - 1)
        count += 1
        if done:
            if reward > env.reward_target:
                success = True
                tot_reward_val.append(episode_reward)
                tot_len.append(count)
                print("Reward:", episode_reward, "Success?", success)

            else:
                success = False
            count = 1
            episode_reward = 0.0
            obs = env.reset()


    env.update_all_plots()
    # env.fig.show()
    # plt.pause(1)
    env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

    # env.fig2.show()
    # plt.pause(1)
    env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

    # env.fig3.show()
    # plt.pause(1)
    env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

    # env.fig5.show()
    # plt.pause(1)
    env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

    pickle.dump((tot_reward_val, tot_len, env.reward_target),
                open(logdir + '/val_result_tot.p', 'wb'))

    tot_rew_mean_all[i] = np.mean(tot_reward_val)
    tot_rew_std_all[i] = np.std(tot_reward_val)

    tot_len_mean_all[i] = np.mean(tot_len)
    tot_len_std_all[i] = np.std(tot_len)

    tot_target_all[i] = env.reward_target

pickle.dump((tot_rew_mean_all,
             tot_rew_std_all,
             tot_len_mean_all,
             tot_len_std_all,
             tot_target_all,
             rew_frac),
            open(base_dir + 'scan_result.p', 'wb'))


def errorbar(x, y, yerr=None, xerr=None, fmt='o', ecolor=None, elinewidth=0.5, capsize=None, barsabove=False, lolims=False, uplims=False, xlolims=False, xuplims=False, errorevery=1, capthick=None, mfc='w', **kwargs):

    return plt.errorbar(x, y, yerr=yerr, xerr=xerr, fmt=fmt, ecolor=ecolor, elinewidth=elinewidth, capsize=capsize, barsabove=barsabove, lolims=lolims, uplims=uplims, xlolims=xlolims, xuplims=xuplims, errorevery=errorevery, capthick=capthick, mfc=mfc, **kwargs)

print(tot_rew_mean_all)
plt.figure()
errorbar(rew_frac, tot_rew_mean_all, yerr=tot_rew_std_all)
plt.xlabel(scan_name)
plt.ylabel(r'<$\sum_i^{ep} r_i$>$_{val}$')
plt.show()
