import helpers.anomaly_checks as hp
import os
import pickle

path = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/'
files = os.listdir(path)

path_out = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019_cleaned/'
files_clean = os.listdir(path_out)

data = pickle.load(open(path + files[20], 'rb'))
data_c = pickle.load(open(path_out + files_clean[0], 'rb'))
