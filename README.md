# Project for automatic matching with AWAKE electron line

Repository that contains the source files used for all experiments documented
in the paper [arXiv](https://arxiv.org/abs/2209.03183).

The main files are:
 - ```TD3_stable_baseline.py``` is the main script used to train and use a TD3 agent on the AWAKE TL environment
 - The env class is defined in: ```tl_op_env_RL.py```
 - Repository for the VAE models: [GitLab](https://gitlab.cern.ch/awake-projects/awake_btv_vae)
 - CoLab notebook for VAE creation and training [here](https://colab.research.google.com/drive/1Zx8RtVsCwveZPHfC6SIOGuB8wb0DQ2Tr)

 Main slides:
 - [Summary slides of measurements on AWAKE](https://indico.cern.ch/event/875967/contributions/3691399/attachments/1967583/3272017/mlawake_last2019.pdf)
 - [Summary slides of measurements on AWAKE - part 2](https://indico.cern.ch/event/878300/contributions/3700077/attachments/1974614/3286022/AWAKE_eline_ML_summary.pdf)
# Cite:
```
@misc{velotti2022automatic,
      title={Automatic setup of 18 MeV electron beamline using machine learning},
      author={Francesco Maria Velotti and Brennan Goddard and Verena Kain and Rebecca Ramjiawan and Giovanni Zevi Della Porta and Simon Hirlaender},
      year={2022},
      eprint={2209.03183},
      archivePrefix={arXiv},
      primaryClass={physics.acc-ph}

```