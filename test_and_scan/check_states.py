from awake_env.tl_opt_env_RL import tlOptEnv as environment

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

state_type, data_type, state_dof = "explicit", 'vae', 5
encoder_type = 'fixed'
env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type)

