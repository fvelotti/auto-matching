from awake_env.tl_opt_env_RL import tlOptEnv as environment
from spinup import ddpg
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import os
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = "explicit", 'vae', 5
encoder_type = 'fixed'
random_seed = 10

# Random seeds
tf.set_random_seed(random_seed)
np.random.seed(random_seed)

env = environment(None, state_type=state_type, data_type=data_type, state_dof=state_dof, encoder_type=encoder_type,
                  seed=random_seed)

env.action_scaling = 0.06
env.plot_freq = 20
env.save_freq = 200
env.reward_fraction_sigma = 0.6
env.rand_x0 = [0.02, 0.02, 0.02, 0.02, 0.02]
env.reward_target = -0.25

env.max_episode_length = 20

epochs, steps_per_epoch, start_steps = 6, 60, 150
act_noise = 0.03


hyp_para = {'rl_agent': 'DDPG', 'epochs': epochs, 'steps_per_epoch': steps_per_epoch, 'start_steps': start_steps,
            'action_scaling': env.action_scaling, 'reward_frac_sigma': env.reward_fraction_sigma, 'act_noise':
                act_noise, 'target': env.reward_target, 'random_seed': random_seed}

env.save_settings(additional_log=hyp_para)

logger_kwargs = dict(output_dir='data/ddpg_' + env.now_str_all, exp_name='ddpd_am_' + env.now_str_all)

env.reset()
env_fn = lambda: env

# agent = sac(env_fn=env_fn, epochs=epochs, steps_per_epoch=steps_per_epoch, start_steps=start_steps, replay_size=10000)

agent = ddpg(env_fn=env_fn, epochs=epochs, steps_per_epoch=steps_per_epoch,
             logger_kwargs=logger_kwargs,
             start_steps=start_steps, seed=random_seed,
             replay_size=int(100000), act_noise=act_noise, max_ep_len=env.max_episode_length)

# path_plots = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/plots_1_12_19/'
path_plots = '/Users/fvelotti/Dropbox/Awake/Plots/ddpg_auto_matching/'
env.fig.show()
env.fig.savefig(path_plots + hyp_para['rl_agent'] + '_actions_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig2.show()

env.fig2.savefig(path_plots + hyp_para['rl_agent'] +
                '_all_interactions_' + env.now_str_all + '.pdf')
plt.pause(1)

env.fig3.show()
env.fig3.savefig(path_plots + hyp_para['rl_agent'] + '_stats_' +
                env.now_str_all + '.pdf')
plt.pause(1)

env.fig5.show()

env.fig5.savefig(path_plots + hyp_para['rl_agent'] + '_states_' +
                env.now_str_all + '.pdf')
plt.pause(1)

