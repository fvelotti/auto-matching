import pickle
import matplotlib.pyplot as plt
import pandas as pd
import os
from pathlib import Path
import numpy as np

ROOT_DATA = Path("/home/fvelotti/pCloudDrive/Awake/Data/")
# ROOT_DATA = Path("/home/fvelotti/Downloads/")

# INNER_FOLDER = ""
INNER_FOLDER_AUG = "Scans_Aug_2020"
INNER_FOLDER_JUL = "Scans_JuneJuly_2020"
# Load data


data_fake_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_20_14_29_btv_fake_enc/"
            "settings_2020-07-07_21_19_10.480633_btv_implicit.p"
        ),
        "rb",
    )
)

data_implicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_15_07_14_btv_5d_imp/"
            "settings_2020-08-10_16_02_34.394866_btv_implicit.p"
        ),
        "rb",
    )
)

data_explicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_JUL
        / (
            "2020-07-07_18_41_47_btv/"
            "settings_2020-07-07_19_48_10.075874_btv_implicit.p"
        ),
        "rb",
    )
)


def make_df_from_dict(input_dict_stats):
    df_implicit = pd.DataFrame(input_dict_stats)
    return df_implicit


df_fake = make_df_from_dict(data_fake_validation)
df_implicit = make_df_from_dict(data_implicit_validation)
df_explicit = make_df_from_dict(data_explicit_validation)

plt.figure()
plt.subplot(131)
plt.plot(-df_fake.penalty, "--o")

plt.subplot(132)
plt.plot(-df_implicit.penalty, "--o")

plt.subplot(133)
plt.plot(-df_explicit.penalty, "--o")

plt.show()


def plot_image_results(df_test, i_s, axis):
    Xs, Ys = np.meshgrid(
        df_test.iloc[i_s]["btv_data"]["x"], df_test.iloc[i_s]["btv_data"]["y"]
    )
    axis[0].contourf(Xs, Ys, df_test.iloc[i_s]["btv_data"]["image"])
    axis[1].plot(
        df_test.iloc[i_s]["btv_data"]["x"],
        df_test.iloc[i_s]["btv_data"]["pro_x"],
        label="x",
    )
    axis[2].plot(
        df_test.iloc[i_s]["btv_data"]["y"],
        df_test.iloc[i_s]["btv_data"]["pro_y"],
        label="y",
    )


i_s_fake, i_e_fake = (21, 24)
i_s_implicit, i_e_implicit = (11, 16)
i_s_explicit, i_e_explicit = (54, 57)


fig, axis = plt.subplots(nrows=2, ncols=2, figsize=(4, 3))
plot_image_results(df_fake, i_s_fake, [axis[0, 0], axis[1, 0], axis[1, 1]])
plot_image_results(df_fake, i_e_fake, [axis[0, 1], axis[1, 0], axis[1, 1]])
axis[0, 0].set(xlabel="x / mm", ylabel="y / mm")
axis[0, 1].set(xlabel="x / mm", ylabel="y / mm")
axis[1, 0].set(xlabel="x / mm", ylabel="Counts (#)")
axis[1, 1].set(xlabel="y / mm", ylabel="Counts (#)")
fig.savefig("/home/fvelotti/awake_paper/images/validation_fake_btv.pdf")

fig, axis = plt.subplots(nrows=2, ncols=2, figsize=(4, 3))
plot_image_results(df_implicit, i_s_implicit, [axis[0, 0], axis[1, 0], axis[1, 1]])
plot_image_results(df_implicit, i_e_implicit, [axis[0, 1], axis[1, 0], axis[1, 1]])
axis[0, 0].set(xlabel="x / mm", ylabel="y / mm")
axis[0, 1].set(xlabel="x / mm", ylabel="y / mm")
axis[1, 0].set(xlabel="x / mm", ylabel="Counts (#)")
axis[1, 1].set(xlabel="y / mm", ylabel="Counts (#)")
fig.savefig("/home/fvelotti/awake_paper/images/validation_implicit_btv.pdf")

fig, axis = plt.subplots(nrows=2, ncols=2, figsize=(4, 3))
plot_image_results(df_explicit, i_s_explicit, [axis[0, 0], axis[1, 0], axis[1, 1]])
plot_image_results(df_explicit, i_e_explicit, [axis[0, 1], axis[1, 0], axis[1, 1]])
axis[0, 0].set(xlabel="x / mm", ylabel="y / mm")
axis[0, 1].set(xlabel="x / mm", ylabel="y / mm")
axis[1, 0].set(xlabel="x / mm", ylabel="Counts (#)")
axis[1, 1].set(xlabel="y / mm", ylabel="Counts (#)")
fig.savefig("/home/fvelotti/awake_paper/images/validation_explicit_btv.pdf")
plt.show()
