import numpy as np
import os
import pandas as pd
import tensorflow as tf
from itertools import product
from awake_env.tl_opt_env_RL import tlOptEnv as environment
# %% ===================================
import datetime
from tqdm import tqdm

tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

# %% ===================================
state_type, data_type, state_dof = 'implicit', 'btv', 5

now = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_').split(".")[0]

charge = 150
btv = 'btv53'

btvs = {'btv53': 'TT41.BTV.412353.LASER', 'btv54': 'BTV54'}

btv_name = btvs[btv]
logdir = f'data/{now}_{data_type}_scan_state_{charge:d}pC_{btv}/'
os.mkdir(logdir)

# %% ===================================
encoder_type = 'fixed'
random_seed = 0

action_scaling = 1.0
plot_freq = 2
no_plot = False
save_freq = 10
reward_fraction_sigma = 0.7
rand_x0 = [0.0] * 5
max_episode_length = 100

reward_target_start = -0.01

x0_scaling = 1

stats = 2
scan_space = np.linspace(0.8, 1.2, 3, endpoint=True)
data_set = [scan_space] * len(rand_x0)
tot_iter = (len(scan_space)**len(data_set)) * stats
print('Iterations for this scan: ', tot_iter)
# %% ===================================
env = environment(None, state_type=state_type,
                  data_type=data_type, state_dof=state_dof, seed=random_seed,
                  encoder_type=encoder_type, action_space_type='sb',
                  no_plot=no_plot, btv_name=btv_name)

n_actions = env.action_space.shape[-1]

env.set_log_path({'path': logdir})
env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0

env.x0_scaling = x0_scaling
env.set_x0()

env.reward_target = reward_target_start

hyp_params = {'rl_agent': 'action_scan',
              'action_scaling': env.action_scaling,
              'reward_fraction_sigma': env.reward_fraction_sigma,
              'reward_target': env.reward_target,
              'random_seed': random_seed,
              'iterations': tot_iter,
              'scan_space': scan_space,
              'btv': btv_name
              }

env.save_settings(additional_log=hyp_params)

env.off_set = np.zeros(5)
s0 = env.reset()

with tqdm(total=tot_iter) as pbar:
    for ele in product(*data_set):
        for _ in range(stats):
            env.off_set = np.zeros(5)
            action = env.x0 * np.array(ele)
            data = env.step(action)
            pbar.update(1)
