import pickle
import matplotlib.pyplot as plt
import pandas as pd
import os
from pathlib import Path
import numpy as np

ROOT_DATA = Path("/home/fvelotti/pCloudDrive/Awake/Data/")
# ROOT_DATA = Path("/home/five loti/Downloads/")

# INNER_FOLDER = ""
INNER_FOLDER_AUG = "Scans_Aug_2020"
INNER_FOLDER_JUL = "Scans_JuneJuly_2020"
# Load data


stats_implicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-12_09_54_56_btv_val_200pC/"
            "train_stats_2020-08-12_09_54_56.552355_btv_implicit5.p"
        ),
        "rb",
    )
)


stats_explicit_validation = pickle.load(
    open(
        ROOT_DATA
        / INNER_FOLDER_AUG
        / (
            "2020-08-10_11_18_05_btv_val_from_july_val_fake/"
            "train_stats_2020-08-10_11_18_07.586939_btv_implicit5.p"
        ),
        "rb",
    )
)


def make_df_from_dict(input_dict_stats):
    df_implicit = pd.DataFrame(input_dict_stats).T
    df_implicit["len"] = df_implicit["total"].apply(lambda x: len(x))
    df_implicit["intensity0"] = df_implicit["intensity"].apply(lambda x: x[0])
    df_implicit["size0"] = df_implicit["size"].apply(lambda x: x[0])
    df_implicit["total0"] = df_implicit["total"].apply(lambda x: x[0])
    df_implicit["intensity"] = df_implicit["intensity"].apply(lambda x: x[-1])
    df_implicit["size"] = df_implicit["size"].apply(lambda x: x[-1])
    df_implicit["total"] = df_implicit["total"].apply(lambda x: x[-1])
    df_implicit["delta_rew"] = df_implicit.apply(
        lambda x: x.total - x.total0, axis=1
    )
    return df_implicit


df_explicit_valid = make_df_from_dict(stats_explicit_validation)

df_implicit_valid = make_df_from_dict(stats_implicit_validation)



fig, axis = plt.subplots(nrows=1, ncols=1)
axis.plot(
    df_implicit_valid.total - df_implicit_valid.total0,
    label="Implicit state encoding",
)
axis.plot(
    df_explicit_valid.total - df_explicit_valid.total0,
    label="Explicit state encoding",
)
axis.set(ylabel=r"$\Delta r_{ep}$", xlabel="Episode #")
axis.axhline(0, ls="--")
axis.legend()
axis.annotate("(b)", xy=(20, 0.8))
# fig.savefig("/home/fvelotti/awake_prab_paper/images/validation_all.pdf")
plt.show()