import pickle
import os

file_name = "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Aug_2020/2020-08-10_15_07_14_btv_5d_imp/data_scan_2020-08-10_15_39_42.539555_btv_implicit5.p"


def clean_settings(file_name):
    settings = pickle.load(
        open(
            file_name,
            "rb",
        )
    )

    if "action_noise" in settings.keys():
        settings.pop("action_noise")
        for key in settings.keys():
            print(key, type(settings[key]))

        pickle.dump(settings, open(file_name, "wb"))
        print("file overwritten")
    else:
        print("no action key found. ")


clean_settings(file_name=file_name)
