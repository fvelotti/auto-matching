import pickle
import matplotlib.pyplot as plt
import pandas as pd
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import numpy as np


def get_data(file_name):
    # Load data
    data = pickle.load(open(file_name, 'rb'))
    data = pd.DataFrame(data)

    # Instantiate env object for normalisation purposes
    state_type, data_type, state_dof = 'implicit', 'vae', 7

    encoder_type = 'fixed'
    random_seed = 0

    env = environment(None, state_type=state_type,
                      data_type=data_type, state_dof=state_dof, seed=random_seed,
                      encoder_type=encoder_type, action_space_type='sb', no_plot=True)

    env.reward_fraction_sigma = 0.6

    data['is_episode_over'] = data.apply(lambda x: np.any((env.normData(x[env.devices]) > 1)) or
           np.any(env.normData(x[env.devices]) < 0) or x['penalty'] < -env.reward_target, axis=1)

    data['size_r'] = data.apply(lambda x: (env.r0 - np.sqrt(np.sum(
        np.array((x['btv_data']['sigma_x'], x['btv_data']['sigma_y'])) ** 2))) / env.r_max, axis=1)

    data['int_r'] = data.apply(lambda x: (np.sum(x['btv_data']['image']) - env.i0) / env.i0, axis=1)

    data['reward'] = data.apply(lambda x: x['size_r'] * env.reward_fraction_sigma + x['int_r'] * (1 -
                                                                                env.reward_fraction_sigma), axis=1)

    devs_real = [ele + '_real' for ele in env.devices]

    data_real = np.zeros((len(data), len(env.devices)))
    is_norm = np.max(data[env.devices[0]]) < 150
    print('isNorm = ', is_norm)
    for i in range(len(data)):
        if is_norm:
            data_real[i, :] = env.invNormData(data[env.devices].iloc[i])
        else:
            data_real[i, :] = data[env.devices].iloc[i]

    for i, ele in enumerate(devs_real):
        data[ele] = data_real[:, i]

    return data, env


def fix_prediction(y_pred, delta=10):
    new_pred = np.zeros_like(y_pred)
    for i in range(0, len(y_pred), delta):

        data_sub = y_pred[i:i + delta]
        mean = np.mean(data_sub)
        if mean > -0.4:
            new_pred[i:i + delta] = np.ones_like(data_sub)
        # elif -0.3 <= mean <= 0.1:
        #     new_pred[i:i + delta] = data_sub
        else:
            new_pred[i:i + delta] = np.zeros_like(data_sub) - 1
    return new_pred


def filter_faults(path, file_name, write=True, path_out='', plot=True, plot_location=''):
    try:
        data, env = get_data(path + file_name)
    except:
        return pd.DataFrame()

    devs_real = [ele + '_real' for ele in env.devices]
    clf = pickle.load(open('/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/isolation_forest_nov.pkl', 'rb'))

    X = data[devs_real + ['size_r', 'int_r', 'reward']]
    y_pred = clf.predict(X)
    y_pred = fix_prediction(y_pred)
    data_filtered = data[y_pred == 1]

    if write:
        pickle.dump(data_filtered, open(path_out + file_name[:-2] + '_filtered.p', 'wb'))

    if plot:
        plt.figure()
        plt.title(plot_location.replace('_', '-').split('/')[-1], fontsize=4)
        plt.plot(data['reward'], '-o', color='k', alpha=0.3, label='reward')
        plt.plot(data['size_r'], '.', label='r$_\sigma$')
        plt.plot(data['int_r'], '.', label='r$_I$')

        plt.plot(y_pred, 'o', label='prediction')
        plt.axhline(env.reward_target, ls='--')
        plt.ylim(-2, 1)
        plt.legend(frameon=True)
        plt.ylabel('Reward')

        plt.savefig(plot_location)
        print(plot_location)

    return data_filtered
