import gym
import numpy as np
import logging.config
from utils_data.dataSource import Btv
import time

class tlOptEnv(gym.Env):
    """
    Define a simple crystal environment.
    The environment defines which actions can be taken at which point and
    when the agent receives which reward.
    """
    def __init__(self, japc, btv_name, devices):
        self.dof = len(devices)
        self.max_episode_length = 10
        self.__version__ = "0.0.1"
        logging.info("zsEnvironment - Version {}".format(self.__version__))

        # action space
#        high = np.ones(self.dof) * np.array([1900e-6, 70e-3, 43e-3])
#        low = np.ones(self.dof) * np.array([1600e-6, 66e-3, 40e-3])
#        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)

        # state space
#        high = np.ones(self.dof) * 2
#        low = np.ones(self.dof) * -2
#        self.observation_space = spaces.Box(low, high, dtype=np.float32)

        self.curr_episode = -1
        self.action_episode_memory = []

        self.curr_step = -1
        self.is_finalized = False
        self.counter = 0

        self.japc = japc

        self.model = Btv(japc=self.japc, btv_name=btv_name)
        self.devices = devices
        
        # Minimum size achievable
        self.r0 = np.sqrt(0.25 ** 2 + 0.25 ** 2)

        # Max intensity
        self.i0 = 1.3e6

        # Max size
        self.r_max = np.sqrt(3 ** 2 + 3 ** 2)
        # reward sharing: fraction on beam size
        self.reward_fraction_sigma = 0.8

    def seed(self, seed):
        np.random.seed(seed)

    def step(self, action):
        """
        The agent takes a step in the environment.
        Parameters
        ----------
        action : int
        Returns
        -------
        observable, reward, episode_over, info : tuple
            observable (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
        # if self.is_finalized:
        #    raise RuntimeError("Episode is done")
        self.curr_step += 1
        state, reward = self._take_action(action)
        return state, reward, self.is_finalized, self.data_btv

    def _take_action(self, action):
        scale = 1e0

        self.counter += 1
        remaining_steps = self.max_episode_length - self.counter
        episode_is_over = (remaining_steps <= 0)

        action = action*scale
        print('action inner ', action)
        print('state inner ', self.state)
        self.state = action

        self.reward = self._get_reward()

        # if self.reward>-7:
        #     self.reward = 50
        #     episode_is_over = True

        if episode_is_over:
            self.is_finalized = True

        return self.state, self.reward

    def set_to_initial(self):
        self.state = np.zeros(self.dof)

    def _get_reward(self):
        scale = 1
        state = self.state.reshape(1, -1)
        print('state before predition: ', state)
        self.set_actors(state)

        # check that state is what is required
        self.sx, self.sy, self.data_btv = self.model.get_image_data(nShot=2, rms=False)
        
        self.sigmas = (self.sx, self.sy)
        self.r = (self.r0 - np.sqrt(np.sum(np.array(self.sigmas) ** 2))) / self.r_max
        self.intensity = (np.sum(self.data_btv['image']) - self.i0) / self.i0
        self.intensity = np.min(self.intensity, 0)

        # Normalisation of reward as combination of beam size and intensity from BTV
        reward = self.r * self.reward_fraction_sigma + self.intensity * (1 - self.reward_fraction_sigma)

        print('Reward: ', reward)
        return reward

    def reset(self):
        """
        Reset the state of the environment and returns an initial observation.
        Returns
        -------
        observation (object): the initial observation of the space.
        """
        print('Reset called')
        self.curr_episode += 1
        self.is_finalized = False
        self.counter = 0
        self.state = np.zeros(self.dof)

        return self.state

    def render(self, mode='human'):
        pass

    def get_actors(self):
        pass

    def set_actors(self, state):
        # sole 1
        # sole 2
        # corr1 x, y
        # corr2 x, y
        # quad1
        # quad2
        # quad3
        for i, device in enumerate(self.devices):
            if 'logical' in device:
                self.set_K(device, state[0][i])
            else:
                print('value to set: ', state[0][i])
                self.set_current(device, state[0][i])
        time.sleep(1.2)

    def set_virtual_parameter(self, device, value):
        ele = self.japc.getParam(device, noPyConversion=True, timingSelectorOverride='').getValue()
        ele.double = value
        self.japc.setParam(device, ele, timingSelectorOverride='')
        print(device + ' set to: ' + str(value))
        time.sleep(0.5)

    def set_current(self, device, value):
        self.japc.setParam(device + '/SettingPPM#current', value, timingSelectorOverride='')
        print(device + ' set to: ' + str(value))

    def set_K(self, device, value):
        self.japc.setParam(device + '/K', value, timingSelectorOverride='')
        print(device + ' set to: ' + str(value))
