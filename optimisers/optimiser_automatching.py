import numpy as np
import os
import pandas as pd
import tensorflow as tf
from itertools import product
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import datetime
import pybobyqa
import matplotlib.pyplot as plt

tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

state_type, data_type, state_dof = "implicit", "btv", 5

now = (
    str(datetime.datetime.now())
    .replace(" ", "_")
    .replace(":", "_")
    .split(".")[0]
)

charge = 150
btv = "btv54"

btvs = {"btv53": "TT41.BTV.412353.LASER", "btv54": "BTV54"}

btv_name = btvs[btv]
logdir = f"data/{now}_{data_type}_optimiser_{charge:d}pC_{btv}/"
os.mkdir(logdir)

encoder_type = "fixed"
random_seed = 0

# This is for the environment only!
number_actors = 5

action_scaling = 1.0
plot_freq = 1
no_plot = False
save_freq = 200
reward_fraction_sigma = 1
rand_x0 = [0.0] * number_actors
max_episode_length = 100

reward_target_start = 10

x0_scaling = 1
env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
    no_plot=no_plot,
    btv_name=btv_name,
)

n_actions = env.action_space.shape[-1]

env.set_log_path({"path": logdir})
env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0

env.x0_scaling = x0_scaling
env.set_x0()

env.reward_target = reward_target_start

hyp_params = {
    "rl_agent": "action_scan",
    "action_scaling": env.action_scaling,
    "reward_fraction_sigma": env.reward_fraction_sigma,
    "reward_target": env.reward_target,
    "random_seed": random_seed,
    "btv": btv_name,
}

env.save_settings(additional_log=hyp_params)


class EnvWrapperQuads:
    def __init__(
        self, env: environment, num_actors: int, target_size: list
    ) -> None:
        self.env = env
        self.dof = num_actors
        self.num_actors_env = 5
        self.env.r0 = np.sqrt(target_size[0] ** 2 + target_size[1] ** 2)
        self.env.off_set = np.zeros(number_actors)
        s0 = self.env.reset()
        self.x0 = env.x0
        self.history = {}

    def _save_history(self):
        if self.history:
            self.history["actions"] = np.vstack(
                [self.history["actions"], self.actions]
            )

            self.history["penalty"] = np.append(
                self.history["penalty"], self.penalty
            )
        else:
            self.history["actions"] = self.actions
            self.history["penalty"] = np.array([self.penalty])

    def objective(self, action):
        full_action = np.concatenate([self.x0[:2], action])
        self.env.off_set = np.zeros(self.num_actors_env)
        _, reward, _, _ = self.env.step(full_action)
        self.actions = self.env.action_real.squeeze()[2:]
        print("Optimiser penalty: ", -reward)
        self.penalty = -reward
        self._save_history()
        return -reward


dof_optimiser = 3
env_wrapper = EnvWrapperQuads(env, dof_optimiser, [0.22, 0.2])

# action = [0.5, 0.5, 0.5]
# env_wrapper.objective(action)
# env_wrapper.env.action_real
# quit()


def general_constraint_norm(x):
    check = x > 1
    check2 = x < -1
    if any(check) or any(check2):
        return -1
    else:
        return 1


lower = -1 * np.ones(dof_optimiser)
upper = np.ones(dof_optimiser)
rhobeg = 0.6

res = pybobyqa.solve(
    env_wrapper.objective,
    env_wrapper.x0[2:],
    seek_global_minimum=False,
    rhobeg=rhobeg,
    objfun_has_noise=True,
    rhoend=1e-4,
    bounds=(lower, upper),
    maxfun=150,
)

env_wrapper.objective(res.x)
env_wrapper.objective(res.x)
plt.figure()
plt.plot(env_wrapper.history["penalty"])

plt.show()

env.update_all_plots()

env.fig.show()
plt.pause(1)
env.fig.savefig(logdir + "/actions" + env.now_str_all + ".png")

env.fig2.show()
plt.pause(1)
env.fig2.savefig(
    logdir + "/all_interactions_" + env.now_str_all + ".png"
)

env.fig3.show()
plt.pause(1)
env.fig3.savefig(logdir + "/stats_" + env.now_str_all + ".png")

env.fig5.show()
plt.pause(1)
env.fig5.savefig(logdir + "/states_" + env.now_str_all + ".png")
