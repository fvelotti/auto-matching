import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import numpy as np
import accphylib.acc_library as al
import os
from pathlib import Path

model_path = "trained_model/new_models/"

# DATA_PATH = Path("/home/fvelotti/pCloudDrive/Awake/SimResults/")
DATA_PATH = Path("data")
DATA_PATH_pcloud = Path("/home/fvelotti/pCloudDrive/Awake/SimResults/")

list_all_models = os.listdir(model_path)

encoders = list(
    filter(lambda x: "encoder" in x and not "Fake" in x, list_all_models)
)
decoders = list(
    filter(lambda x: "decoder" in x and not "Fake" in x, list_all_models)
)

decoders.sort()
encoders.sort()

x_enc = [f"VAE_{i}" for i in range(1, len(encoders) + 1)]
x_dec = [f"DFA_{i}" for i in range(1, len(decoders) + 1)]

# DATA_PATH / "scan_enc_2020-06-26_09_39_25.685480/scan_result.p",

data_foder_scan_enc_dec = "scan_enc_2020-06-26_09_39_25.685480"
data = pickle.load(
    open(
        DATA_PATH_pcloud / data_foder_scan_enc_dec / "scan_result.p",
        "rb",
    )
)

(
    tot_rew_mean_all,
    tot_rew_std_all,
    tot_len_mean_all,
    tot_len_std_all,
    tot_target_all,
) = data

fig, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(3, 3))
axes[0, 0].set_title(r"<$\sum_i^{ep} r_i$>$_{ep}$", fontsize=6)
sns.heatmap(
    tot_rew_mean_all,
    xticklabels=x_dec,
    yticklabels=x_enc,
    cmap="viridis",
    ax=axes[0, 0],
)
axes[0, 1].set_title(r"std($\sum_i^{ep} r_i$)$_{ep}$", fontsize=6)
sns.heatmap(
    tot_rew_std_all,
    xticklabels=x_dec,
    yticklabels=x_enc,
    cmap="viridis",
    ax=axes[0, 1],
)

axes[1, 0].set_title(r"<$L_{ep}$>", fontsize=6)
sns.heatmap(
    tot_len_mean_all,
    xticklabels=x_dec,
    yticklabels=x_enc,
    cmap="viridis",
    ax=axes[1, 0],
)
axes[1, 1].set_title(r"std($L_{ep})$", fontsize=6)
sns.heatmap(
    tot_len_std_all,
    xticklabels=x_dec,
    yticklabels=x_enc,
    cmap="viridis",
    ax=axes[1, 1],
)

plt.figure()
plt.title(r"$T_{ep}$", fontsize=10)
sns.heatmap(
    tot_target_all, xticklabels=x_dec, yticklabels=x_enc, cmap="viridis"
)
plt.xticks(rotation=90)
plt.show()

# / "scan_enc_only_all_2020-06-26_15_38_23.970497/scan_result.p",
data_foder_scan = "scan_enc_only_all_2021-12-13_11_14_17.351426"
data = pickle.load(
    open(
        DATA_PATH_pcloud / data_foder_scan / "scan_result.p",
        "rb",
    )
)


(
    tot_rew_mean_all,
    tot_rew_std_all,
    tot_len_mean_all,
    tot_len_std_all,
    tot_target_all,
) = data

data_foder_scan_2 = "scan_enc_only_all_2021-12-13_16_16_01.262865"
data2 = pickle.load(
    open(
        DATA_PATH_pcloud / data_foder_scan_2 / "scan_result.p",
        "rb",
    )
)


(
    tot_rew_mean_all_2,
    tot_rew_std_all_2,
    tot_len_mean_all_2,
    tot_len_std_all_2,
    tot_target_all_2,
) = data2

x_enc = ["Gauss", "W-GAN"] + x_enc[:-1]
print(x_enc)


fig, axis = plt.subplots(nrows=3, ncols=1, figsize=(3, 4), sharex=True)

al.errorbar(
    np.arange(len(tot_rew_mean_all)),
    tot_rew_mean_all,
    yerr=tot_rew_std_all,
    ax=axis[0],
    label=r"$\alpha$ = 0.05",
)
al.errorbar(
    np.arange(len(tot_rew_mean_all_2)),
    tot_rew_mean_all_2,
    yerr=tot_rew_std_all_2,
    ax=axis[0],
    label=r"$\alpha$ = 0.1",
)
axis[0].axhline(0, ls="--", color="grey")
axis[0].set_ylabel(r"<$r_{ep}$>")
axis[0].legend(
    frameon=True,
    bbox_to_anchor=(0.0, 1.02, 1.0, 0.102),
    loc="lower left",
    ncol=2,
    mode="expand",
    borderaxespad=0.0,
)

al.errorbar(
    np.arange(len(tot_len_mean_all)),
    tot_len_mean_all,
    yerr=tot_len_std_all,
    ax=axis[1],
)
al.errorbar(
    np.arange(len(tot_len_mean_all_2)),
    tot_len_mean_all_2,
    yerr=tot_len_std_all_2,
    ax=axis[1],
)
axis[1].set_ylabel(r"<$L_{ep}$>")

p0 = axis[2].plot(
    np.arange(len(tot_target_all)),
    tot_target_all,
    "o--",
)
axis[2].axhline(
    np.mean(tot_target_all),
    ls="--",
    color=p0[0].get_color(),
    label=f"{np.mean(tot_target_all):1.2f}",
)
p1 = axis[2].plot(
    np.arange(len(tot_target_all_2)),
    tot_target_all_2,
    "o--",
)
axis[2].axhline(
    np.mean(tot_target_all_2),
    ls="--",
    color=p1[0].get_color(),
    label=f"{np.mean(tot_target_all_2):1.2f}",
)
axis[2].set_xticks(np.arange(len(tot_rew_mean_all)))
axis[2].set_xticklabels(x_enc)
labels = axis[2].get_xticklabels()
plt.setp(labels, rotation=90, horizontalalignment="right")
axis[2].set_ylabel(r"<$T_{ep}$>")
axis[2].legend(
    frameon=True,
    bbox_to_anchor=(0.0, 1.02, 1.0, 0.102),
    loc="lower left",
    ncol=2,
    mode="expand",
    borderaxespad=0.0,
)

plt.savefig("/home/fvelotti/awake_prab_paper/images/vae_scans.pdf")
plt.show()


quit()

data = pickle.load(
    open(
        "./data/scan_enc_only_zShape_2020-06-26_14_17_43.430622/scan_result.p",
        "rb",
    )
)

(
    tot_rew_mean_all,
    tot_rew_std_all,
    tot_len_mean_all,
    tot_len_std_all,
    tot_target_all,
) = data

x_enc = [512, 64]

plt.figure(figsize=(5.5, 1.8))
plt.subplot(1, 3, 1)
al.errorbar(
    np.arange(len(tot_rew_mean_all)), tot_rew_mean_all, yerr=tot_rew_std_all
)
plt.axhline(np.mean(tot_rew_nom))
plt.xticks(np.arange(len(tot_rew_mean_all)), x_enc)
plt.xticks(rotation=90)
plt.ylabel(r"<$\sum_i^{ep} r_i$>$_{ep}$")

plt.subplot(1, 3, 2)
al.errorbar(
    np.arange(len(tot_len_mean_all)), tot_len_mean_all, yerr=tot_len_std_all
)
plt.axhline(np.mean(tot_len_nom))
plt.xticks(np.arange(len(tot_rew_mean_all)), x_enc)
plt.xticks(rotation=90)
plt.ylabel(r"<$L_{ep}$>")

plt.subplot(1, 3, 3)
plt.plot(np.arange(len(tot_target_all)), tot_target_all, "o--")
plt.axhline(target_nom)
plt.xticks(np.arange(len(tot_rew_mean_all)), x_enc)
plt.xticks(rotation=90)
plt.ylabel(r"<$T_{ep}$>")
plt.show()

data = pickle.load(
    open("./data/scan_reward_frac_2020-06-30_19_10_44/scan_result.p", "rb")
)

(
    tot_rew_mean_all,
    tot_rew_std_all,
    tot_len_mean_all,
    tot_len_std_all,
    tot_target_all,
    rew_frac,
) = data


plt.figure(figsize=(5.5, 1.8))
plt.subplot(1, 3, 1)
al.errorbar(rew_frac, tot_rew_mean_all, yerr=tot_rew_std_all)
plt.ylabel(r"<$\sum_i^{ep} r_i$>$_{ep}$")

plt.subplot(1, 3, 2)
al.errorbar(rew_frac, tot_len_mean_all, yerr=tot_len_std_all)
plt.ylabel(r"<$L_{ep}$>")

plt.subplot(1, 3, 3)
plt.plot(rew_frac, tot_target_all, "o--")
plt.plot(rew_frac, tot_target_all[0] / rew_frac[0] * rew_frac, "o--")
plt.ylabel(r"<$T_{ep}$>")
plt.show()
