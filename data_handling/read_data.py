import pickle
import matplotlib.pyplot as plt
import numpy as np
import os

# %% ---------------------------------------

path = "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Dec_2019/"
files = os.listdir(path)

for file_name in files:
    if "train_stats" in file_name and "btv" in file_name:
        data_s = pickle.load(open(path + file_name, "rb"))
        episodes = max(data_s.keys()) + 1
        print(file_name)
        print(episodes)

        lens = np.zeros(episodes)
        start_r = np.zeros(episodes)
        end_r = np.zeros(episodes)
        mean_r = np.zeros(episodes)

        for i, ep in enumerate(data_s.keys()):
            lens[i] = len(data_s[ep]["total"])
            start_r[i] = data_s[ep]["total"][0]
            end_r[i] = data_s[ep]["total"][-1]
            mean_r[i] = np.sum(data_s[ep]["total"][1:])

        plt.figure()
        plt.title(file_name.replace("_", "-"))
        plt.plot(mean_r)

# %% ---------------------------------------
data_s = pickle.load(
    open(
        "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Dec_2019/train_stats_2019-12-15_20_53_25.560093_btv_explicit5.p",
        "rb",
    )
)
# '/home/fvelotti/Dropbox/Awake/Data/Scans_Dec_2019/train_stats_2019-12-15_15_39_30.346562_btv_implicit5.p', 'rb'))

# settings = pickle.load(
#     open(
#         "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Dec_2019/settings_2019-12-15_17_25_56.698812_btv_implicit5.p",
#         "rb",
#     )
# )

episodes = max(data_s.keys()) + 1
lens = np.zeros(episodes)
start_r = np.zeros(episodes)
end_r = np.zeros(episodes)
mean_r = np.zeros(episodes)

for i, ep in enumerate(data_s.keys()):
    lens[i] = len(data_s[ep]["total"])
    start_r[i] = data_s[ep]["total"][0]
    end_r[i] = data_s[ep]["total"][-1]
    mean_r[i] = sum(data_s[ep]["total"][1:])

plt.figure()
plt.plot(mean_r, c="royalblue")
plt.xlabel("Episodes")
plt.ylabel("$\sum_t R_t$")
# plt.axhline(settings['reward_target'], ls='--')
# plt.savefig(
#     "/home/fvelotti/Dropbox/Awake/Plots/Dec_19_run_analysis/Sum_reward_TD3_expl_dangling_training.pdf"
# )

plt.show()