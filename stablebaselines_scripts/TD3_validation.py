from matplotlib import pyplot as plt
import numpy as np
import pickle
import os
from keras.models import load_model
from trained_model import trainedmodel

import tensorflow as tf
import pandas as pd
from stable_baselines import TD3
from stable_baselines.td3.policies import MlpPolicy
from stable_baselines.ddpg.noise import NormalActionNoise
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import datetime

tf_config = tf.ConfigProto()
tf_config.gpu_options.allow_growth = True
sess = tf.Session(config=tf_config)

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = 'implicit', 'btv', 5

now = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_').split(".")[0]
logdir = f'data/{now}_{data_type}_val_200pC/'
os.mkdir(logdir)

model_dir = 'data/2020-08-10_15_07_14_btv_5d_imp'
model_path = 'trained_model/new_models/encoder_AWAKE_multiple_VAE_Fake_1.hdf5'

all_files = os.listdir(model_dir)
sett_files = [ele for ele in all_files if 'data_scan' in ele]

settings = pickle.load(open(os.path.join(model_dir, sett_files[-1]), 'rb'))

encoder_type = 'fixed'
random_seed = 0

action_scaling = settings['action_scaling']
plot_freq = 1
no_plot = False
save_freq = 10
reward_fraction_sigma = settings['reward_fraction_sigma']
rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
max_episode_length = 100

reward_target_start = settings['reward_target']
learning_starts = 100

x0_scaling = 0.8


class NewModel(trainedmodel.TrainedModel):
    def _loadFiles(self):
        self.encoderModel = load_model(model_path, compile=False)

        self.decoderModel = load_model(self.path + 'decoder' + self.fn, compile=False)
        self.zpredictorModel = load_model(self.path + 'zpredictor' + self.fn, compile=False)

# new_model = {'model': NewModel(None, state_dof)}
new_model = {}
env = environment(None, state_type=state_type,
                  data_type=data_type, state_dof=state_dof, seed=random_seed,
                  encoder_type=encoder_type, action_space_type='sb',
                  no_plot=no_plot, new_model=new_model)

n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

env.set_log_path({'path': logdir})
env.action_scaling = action_scaling
env.plot_freq = plot_freq
env.save_freq = save_freq
env.reward_fraction_sigma = reward_fraction_sigma
env.rand_x0 = rand_x0

env.x0_scaling = x0_scaling
env.set_x0()

env.reward_target = reward_target_start

# Load saved model
model = TD3(MlpPolicy, env, action_noise=action_noise, verbose=1, learning_starts=learning_starts,
            seed=random_seed, n_cpu_tf_sess=1)

print('Load model')
model = model.load(model_dir + '/model.h5', env=env)

obs = env.reset()

# Evaluate the agent
tot_reward_val = []
tot_len = []
final_action = []
episode_reward = 0
count = 1
success = False
for _ in range(30):
      action, _ = model.predict(obs)
      obs, reward, done, info = env.step(action)
      # env._render_per_interation()
      episode_reward += reward
      count += 1
      if reward > env.reward_target:
          success = True
          episode_reward += 10
          tot_reward_val.append(episode_reward)
          tot_len.append(count)
          final_action.append(env.get_actions())

          count = 1
          episode_reward = 0.0
      if done:
              print("Reward:", episode_reward, "Success?", success)
              success = False
              obs = env.reset()

pickle.dump((tot_reward_val, tot_len, env.reward_target),
            open(logdir + '/train_result.p', 'wb'))

env.update_all_plots()
# env.fig.show()
# plt.pause(1)
env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

# env.fig2.show()
# plt.pause(1)
env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

# env.fig3.show()
# plt.pause(1)
env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

# env.fig5.show()
# plt.pause(1)
env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

