import pickle
import matplotlib.pyplot as plt
import pandas as pd
from awake_env.tl_opt_env_RL import tlOptEnv as environment
import os
import numpy as np

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"
# %% ---------------------------------------

# Load data
stats = pickle.load(
    open(
        (
            "/home/fvelotti/pCloudDrive/"
            "Awake/Data/Scans_Aug_2020/2020-08-10_15_07_14_btv_5d_imp/"
            "train_stats_2020-08-10_15_07_14.900352_btv_implicit5.p"
        ),
        "rb",
    )
)
settings = pickle.load(
    open(
        "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Aug_2020/2020-08-10_15_07_14_btv_5d_imp/data_scan_2020-08-10_15_07_14.900352_btv_implicit5.p",
        "rb",
    )
)
data = pickle.load(
    open(
        "/home/fvelotti/pCloudDrive/Awake/Data/Scans_Aug_2020/2020-08-10_15_07_14_btv_5d_imp/settings_2020-08-10_15_07_14.900352_btv_implicit.p",
        "rb",
    )
)

data = pd.DataFrame(data)


# %% ---------------------------------------
def get_start_end_stats(stats_df, reward):
    episodes = max(stats_df.keys()) + 1
    lens = np.zeros(episodes)
    start_r = np.zeros(episodes)
    end_r = np.zeros(episodes)

    for i, ep in enumerate(stats_df.keys()):
        lens[i] = len(stats_df[ep][reward])
        start_r[i] = stats_df[ep][reward][0]
        end_r[i] = stats_df[ep][reward][-1]

    start = start_r[(end_r > -10) & (start_r > -10)]
    end = end_r[(end_r > -10) & (start_r > -10)]

    return start, end


# %% ---------------------------------------
# Instantiate env object for normalisation purposes
state_type, data_type, state_dof = (
    settings["state_type"],
    "vae",
    settings["state_dof"],
)

encoder_type = settings["encoder_type"]
random_seed = settings["random_seed"]

env = environment(
    None,
    state_type=state_type,
    data_type=data_type,
    state_dof=state_dof,
    seed=random_seed,
    encoder_type=encoder_type,
    action_space_type="sb",
)

# Divide dataset per episode
treshold = settings["reward_fraction_sigma"]

# Get actors
actors = env.devices
# %% ---------------------------------------

env.reward_fraction_sigma = settings["reward_fraction_sigma"]

data["is_episode_over"] = data.apply(
    lambda x: np.any((env.normData(x[env.devices]) > 1))
    or np.any(env.normData(x[env.devices]) < -1)
    or x["penalty"] < -env.reward_target,
    axis=1,
)

data["size_r"] = data.apply(
    lambda x: (
        env.r0
        - np.sqrt(
            np.sum(
                np.array(
                    (x["btv_data"]["sigma_x"], x["btv_data"]["sigma_y"])
                )
                ** 2
            )
        )
    )
    / env.r_max,
    axis=1,
)

data["int_r"] = data.apply(
    lambda x: (np.sum(x["btv_data"]["image"]) - env.i0) / env.i0, axis=1
)

data["reward"] = data.apply(
    lambda x: x["size_r"] * env.reward_fraction_sigma
    + x["int_r"] * (1 - env.reward_fraction_sigma),
    axis=1,
)

# %% ---------------------------------------
plt.figure()
plt.plot(data["reward"], "-o", color="k", alpha=0.3)
plt.plot(data["size_r"], ".")
plt.plot(data["int_r"], ".")
plt.axhline(treshold, ls="--")

plt.ylim(-2, 0)
plt.show()

# %% ---------------------------------------
data_test = data.sort_values("reward")

# %% ---------------------------------------
env.reset()
# %% ---------------------------------------

# # Simulate measured reward with DFA
# env.action_scaling = 1
# env.max_episode_length = 1000

steps = len(data_test)

# reward_dfa = np.zeros(steps)

# for i in range(steps):
#     env.off_set = 0
#     state, rew, _, _ = env.step(data_test.iloc[i][env.devices])
#     reward_dfa[i] = rew

# %% ---------------------------------------
plt.figure()
plt.plot(
    np.arange(len(data_test.iloc[:steps]["penalty"])),
    -data_test.iloc[:steps]["penalty"],
    label="Real AWAKE",
)
# plt.plot(np.arange(steps), reward_dfa, ".", label="Deep fake AWAKE")
plt.xlabel("Iterations")
plt.ylabel("r$_i$")
plt.legend()
# plt.savefig('/home/fvelotti/Dropbox/Awake/Plots/Dec_19_run_analysis/reward_dfa_real.pdf')

plt.figure(figsize=(3, 6))
for i in range(5):
    plt.subplot(5, 1, i + 1)
    plt.plot(np.arange(steps), data_test.iloc[:steps][env.devices[i]])
    plt.ylabel(f"X$_{i}$")
plt.xlabel("Iterations")
# plt.savefig('/home/fvelotti/Dropbox/Awake/Plots/Dec_19_run_analysis/actions_reward_comparison.pdf')

# plt.figure()
# plt.plot(-data_test.iloc[:steps]["penalty"], reward_dfa, "o")
# %% ---------------------------------------
stats = {}
episode = 0
start_ep = 0
for i in range(len(data)):

    if data.iloc[i]["is_episode_over"]:
        stats[episode] = {}
        len_ep = i - start_ep + 1
        print(len_ep)
        stats[episode]["intensity"] = np.zeros(len_ep)
        stats[episode]["size"] = np.zeros(len_ep)
        stats[episode]["total"] = np.zeros(len_ep)

        for j in range(len_ep):
            stats[episode]["total"][j] = (
                -1 * data.iloc[start_ep + j]["penalty"]
            )
            stats[episode]["intensity"][j] = (
                np.sum(data.iloc[start_ep + j]["btv_data"]["image"])
                - env.i0
            ) / env.i0
            sigmas = (
                data.iloc[start_ep + j]["btv_data"]["sigma_x"],
                data.iloc[start_ep + j]["btv_data"]["sigma_y"],
            )
            stats[episode]["size"][j] = (
                env.r0 - np.sqrt(np.sum(np.array(sigmas) ** 2))
            ) / env.r_max

        start_ep = i + 1
        episode += 1
# %% ---------------------------------------

tot_s, tot_e = get_start_end_stats(stats, "total")

plt.figure(figsize=(3, 5))
plt.subplot(311)
plt.hist(tot_s, bins=100, histtype="step", label="Start Ep")
plt.hist(tot_e, bins=100, histtype="step", label="End Ep")
plt.xlabel("Reward total")
plt.ylabel("Counts")
plt.legend()

int_s, int_e = get_start_end_stats(stats, "intensity")
plt.subplot(312)
plt.hist(int_s, bins=100, histtype="step", label="Start Ep")
plt.hist(int_e, bins=100, histtype="step", label="End Ep")
plt.xlabel("Reward intensity")
plt.ylabel("Counts")
plt.legend()

size_s, size_e = get_start_end_stats(stats, "size")
plt.subplot(313)
plt.hist(size_s, bins=100, histtype="step", label="Start Ep")
plt.hist(size_e, bins=100, histtype="step", label="End Ep")
plt.xlabel("Reward $\sqrt{\sigma_x^2 + \sigma_y^2}$")
plt.ylabel("Counts")
plt.legend()
plt.show()

plt.figure(figsize=(3, 5))
plt.subplot(311)
plt.plot(tot_s)
plt.plot(tot_e)
plt.ylabel("Reward total")
plt.legend()

int_s, int_e = get_start_end_stats(stats, "intensity")
plt.subplot(312)
plt.plot(int_s)
plt.plot(int_e)
plt.ylabel("Reward intensity")
plt.legend()

size_s, size_e = get_start_end_stats(stats, "size")
plt.subplot(313)
plt.plot(size_s)
plt.plot(size_e)
plt.ylabel("Reward $\sqrt{\sigma_x^2 + \sigma_y^2}$")
plt.legend()
plt.show()