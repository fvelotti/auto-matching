import helpers.anomaly_checks as hp
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pickle
from scipy.spatial.distance import pdist, squareform
from scipy import exp
from scipy.linalg import eigh
from awake_env.tl_opt_env_RL import tlOptEnv as environment


state_type, data_type, state_dof = 'explicit', 'vae', 5

encoder_type = 'centred'
random_seed = 0

env_e = environment(None, state_type=state_type,
                    data_type=data_type, state_dof=state_dof, seed=random_seed,
                    encoder_type=encoder_type, action_space_type='sb', no_plot=True)


def rbf_kernel_pca(X, gamma, n_components):
    """
    RBF kernel PCA implementation.    
    Parameters
    ------------
    X: {NumPy ndarray}, shape = [n_examples, n_features]  
    gamma: float
        Tuning parameter of the RBF kernel    
    n_components: int
        Number of principal components to return    
    Returns
    ------------
    X_pc: {NumPy ndarray}, shape = [n_examples, k_features]
        Projected dataset   
    """
    # Calculate pairwise squared Euclidean distances
    # in the MxN dimensional dataset.
    sq_dists = pdist(X, 'sqeuclidean')
    # Convert pairwise distances into a square matrix.
    mat_sq_dists = squareform(sq_dists)
    # Compute the symmetric kernel matrix.
    K = exp(-gamma * mat_sq_dists)
    # Center the kernel matrix.
    N = K.shape[0]
    one_n = np.ones((N, N)) / N
    K = K - one_n.dot(K) - K.dot(one_n) + one_n.dot(K).dot(one_n)
    # Obtaining eigenpairs from the centered kernel matrix
    # scipy.linalg.eigh returns them in ascending order
    eigvals, eigvecs = eigh(K)
    eigvals, eigvecs = eigvals[::-1], eigvecs[:, ::-1]
    # Collect the top k eigenvectors (projected examples)
    X_pc = np.column_stack([eigvecs[:, i]
                            for i in range(n_components)])
    return X_pc


path = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019_cleaned/'
files = os.listdir(path)

data_full = []

for file_name in files[:]:
    if 'filtered' in file_name and 'data' in file_name:
        print(file_name)
        data, env = hp.get_data(path + file_name)
        data_full.append(data)


data = pd.concat(data_full, ignore_index=True)

real_actors = [ele + '_real' for ele in env.devices]

x0_real = env.invNormData(env.x0) * 0.6

plt.figure()
for i, action in enumerate(real_actors):
    p0 = plt.plot(data[action], '.')
    plt.axhline(x0_real[i], color=p0[0].get_color())
    plt.axhline(x0_real[i] + (np.abs(x0_real[i]) * 0.3),
                color=p0[0].get_color())
    plt.axhline(x0_real[i] - (np.abs(x0_real[i]) * 0.3),
                color=p0[0].get_color())
plt.show()


def is_almost_x0(actions):
    up = actions < x0_real + (np.abs(x0_real) * 0.3)
    low = actions > x0_real - (np.abs(x0_real) * 0.3)
    summ = np.concatenate([up, low])
    return all(summ)


def is_action_ok(x):
    neg_limits = [0, 0, -35, -35, -35]
    check = (x > env.limits)
    check2 = (x < neg_limits)
    check = any(check) or any(check2)
    return not check


data['isActOK'] = data.apply(lambda x: is_action_ok(x[real_actors]), axis=1)

data_ok = data[data['isActOK']]

g = sns.PairGrid(data_ok[real_actors], despine=False)
g = g.map_upper(sns.scatterplot, color='k', edgecolor=None)
g = g.map_lower(sns.kdeplot)
g = g.map_diag(sns.kdeplot)
plt.show()


def get_states(env, data_btv):
    state = list(env.stateEncoder.encodeImage(data_btv['raw_image']))
    return pd.Series(state)


states_crop = [f's_c{i}' for i in range(env_e.state_dof)]
states_fix = [f's{i}' for i in range(env.state_dof)]

temp_fix = data_ok.apply(lambda x: get_states(env, x['btv_data']), axis=1)
temp_crop = data_ok.apply(lambda x: get_states(env_e, x['btv_data']), axis=1)

data_ok[states_crop] = temp_crop
data_ok[states_fix] = temp_fix

# g = sns.PairGrid(data_ok[states + real_actors], despine=False)
# g = g.map_upper(sns.scatterplot, color='k', edgecolor=None)
# g = g.map_lower(sns.kdeplot)
# g = g.map_diag(sns.kdeplot)
# plt.show()

data_ok.index = data_ok.time

data_ok['day'] = data_ok.index.day

pickle.dump(data_ok, open(
    '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019_cleaned/complete_data_crop_fix.p', 'wb'))

actors_new_names = ['sol1', 'sol2', 'q1', 'q2', 'q3']
actors_norm = [ele + '_norm' for ele in actors_new_names]

col_rename = {ele: new_ele for ele,
              new_ele in zip(real_actors, actors_new_names)}

data_ok = data_ok.rename(columns=col_rename)

data_ok[actors_norm] = data_ok.apply(
    lambda x: pd.Series(env.normData(x[actors_new_names])), axis=1)

plt.rcParams["axes.labelsize"] = 6
plt.rcParams["xtick.labelsize"] = 4
plt.rcParams["ytick.labelsize"] = 4


g = sns.PairGrid(data_ok[states + actors_new_names + ['day']],
                 despine=False,
                 hue='day',
                 height=1.1)

g = g.map_upper(sns.scatterplot, edgecolor=None, alpha=0.5, marker='.')
g = g.map_lower(sns.kdeplot)
g = g.map_diag(sns.kdeplot)
g.fig.subplots_adjust(wspace=.02, hspace=0.02)

plt.show()

data_per_day = [group[1] for group in data_ok.groupby(data_ok.index.day)]

p = 7 + 5
data_ok['l2-action'] = data_ok.apply(lambda x: (
    np.sum(np.abs(x[actors_norm])**p))**(1/p), axis=1)
data_ok['l2-states'] = data_ok.apply(lambda x: (
    np.sum(np.abs(x[states])**p))**(1/p), axis=1)

plt.rcParams["axes.labelsize"] = 10
plt.rcParams["xtick.labelsize"] = 8
plt.rcParams["ytick.labelsize"] = 8
days = np.unique(data_ok.day)

fig, axes = plt.subplots(ncols=len(days), nrows=1, figsize=(7, 1.5))
for ax, day in zip(axes, days):
    ax.set_title(f'{day} November 2019')
    sns.kdeplot(data_ok[data_ok.day == day]['l2-action'],
                data_ok[data_ok.day == day]['l2-states'],
                shade=True, shade_lowest=False,
                # alpha=0.6,
                label=day,
                ax=ax,
                cmap='viridis')
    ax.set(xlim=(np.min(data_ok['l2-action']) * 0.8, np.max(data_ok['l2-action']) * 1.2),
           ylim=(np.min(data_ok['l2-states']) * 0.8,
                 np.max(data_ok['l2-states']) * 1.2),
           xlabel=rf'$L_{{{p}}}$ actions',
           ylabel=rf'$L_{{{p}}}$ states')

plt.show()

X = data_ok[states + actors_norm]

X_pca = rbf_kernel_pca(X, gamma=6, n_components=12)

x_pca_df = pd.DataFrame(X_pca, 
                        index=data_ok.index)

x_pca_df['day'] = data_ok['day']

plt.figure()
sns.scatterplot(x_pca_df[0], x_pca_df[2], hue=x_pca_df['day'], palette='Set1', alpha=0.5)

plt.show()

g = sns.PairGrid(x_pca_df,
                 despine=False,
                 hue='day',
                 height=1.1,
                 )

g = g.map_upper(sns.scatterplot, edgecolor=None, alpha=0.5, marker='.')
g = g.map_lower(sns.scatterplot, edgecolor=None, alpha=0.5)
g = g.map_diag(sns.kdeplot)
g.fig.subplots_adjust(wspace=.02, hspace=0.02)

plt.show()