from stable_baselines import TD3
from stable_baselines.td3.policies import MlpPolicy
from stable_baselines.ddpg.noise import NormalActionNoise
from awake_env.tl_opt_env_RL import tlOptEnv as environment

from matplotlib import pyplot as plt
import numpy as np
import pickle

from keras.models import load_model
from trained_model import trainedmodel
import datetime
import shutil
import os
import tensorflow as tf
tf_config=tf.ConfigProto()
tf_config.gpu_options.allow_growth=True
sess = tf.Session(config=tf_config)
# %% ---------------------------------------

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

state_type, data_type, state_dof = 'implicit', 'vae', 5
encoder_type = 'fixed'
random_seed = 0

action_scaling = 0.1
plot_freq = 50
save_freq = 50
reward_fraction_sigma = 0.8
rand_x0 = [0.05, 0.05, 0.05, 0.05, 0.05]
max_episode_length = 100

reward_target = -0.45 # -0.25336

learning_starts = 100

model_path = 'trained_model/new_models/'
list_all_models = os.listdir(model_path)

encoders = list(filter(lambda x: 'encoder' in x and not 'Fake' in x, list_all_models))
decoders = list(filter(lambda x: 'decoder' in x and not 'Fake' in x, list_all_models))
zpreds = list(filter(lambda x: 'zpred' in x, list_all_models))

decoders.sort()
encoders.sort()
zpreds.sort()

assert len(decoders) == len(encoders) == len(zpreds)

now = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_')
base_dir = f'data/scan_enc_{now}/'

tot_len_mean_all = np.zeros((len(encoders), len(decoders)))
tot_len_std_all = np.zeros((len(encoders), len(decoders)))

tot_rew_mean_all = np.zeros((len(encoders), len(decoders)))
tot_rew_std_all = np.zeros((len(encoders), len(decoders)))

tot_target_all = np.zeros((len(encoders), len(decoders)))

for i, encoder in enumerate(encoders[:]):
    for j, (decoder, pred) in enumerate(zip(decoders[:], zpreds[:])):
        fold_name = f'train_en{i}_de{j}'
        logdir = base_dir + fold_name
        shutil.rmtree(logdir, ignore_errors=True)
        os.makedirs(logdir)

        try:
            del env
        except:
            print('env not there yet.')

        class NewModel(trainedmodel.TrainedModel):
            def _loadFiles(self):
                self.encoderModel = load_model(model_path + encoder, compile=False)
                self.decoderModel = load_model(model_path + decoder, compile=False)
                self.zpredictorModel = load_model(model_path + pred, compile=False)

        new_model = {'model': NewModel(None, state_dof)}

        log_path = {'path': logdir}

        env = environment(None, state_type=state_type,
                          data_type=data_type, state_dof=state_dof, seed=random_seed,
                          encoder_type=encoder_type, action_space_type='sb', 
                          reward_dang=True, new_model=new_model, no_plot=True,
                          log_path=log_path)


        n_actions = env.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

        env.action_scaling = action_scaling
        env.plot_freq = plot_freq
        env.save_freq = save_freq
        env.reward_fraction_sigma = reward_fraction_sigma
        env.rand_x0 = rand_x0
        env.max_episode_length = max_episode_length

        env.reward_target = reward_target
        env.reward_dang_min = env.reward_target

        total_timesteps = 600

        hyp_params = {'rl_agent': 'SB TD3', 'learning_starts': learning_starts, 'total_timesteps': total_timesteps,
                      'action_scaling': env.action_scaling, 'reward_fraction_sigma': env.reward_fraction_sigma,
                      'action_noise': action_noise, 'reward_target': env.reward_target, 'random_seed': random_seed,
                      }

        env.save_settings(additional_log=hyp_params)

        model = TD3(MlpPolicy, env, action_noise=action_noise, verbose=1, learning_starts=learning_starts,
                    seed=random_seed, n_cpu_tf_sess=1)
        model.learn(total_timesteps=total_timesteps, log_interval=20)
        
        env.update_all_plots()
        # env.fig.show()
        # plt.pause(1)
        env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

        # env.fig2.show()
        # plt.pause(1)
        env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

        # env.fig3.show()
        # plt.pause(1)
        env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

        # env.fig5.show()
        # plt.pause(1)
        env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')


        # ==============================================
        # Re-training with new reward
        # ==============================================

        new_target = env.reward_target * 1.05

        del env

        env = environment(None, state_type=state_type,
                          data_type=data_type, state_dof=state_dof, seed=random_seed,
                          encoder_type=encoder_type, action_space_type='sb',
                          new_model=new_model, no_plot=True,
                          log_path=log_path)

        n_actions = env.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

        env.action_scaling = action_scaling
        env.plot_freq = plot_freq
        env.save_freq = save_freq
        env.reward_fraction_sigma = reward_fraction_sigma
        env.rand_x0 = rand_x0
        env.reward_target = new_target
        env.max_episode_length = max_episode_length
        # env.i0 = 1.3e6

        total_timesteps = 300

        hyp_params = {'rl_agent': 'SB TD3', 'learning_starts': learning_starts, 'total_timesteps': total_timesteps,
                      'action_scaling': env.action_scaling, 'reward_fraction_sigma': env.reward_fraction_sigma,
                      'action_noise': action_noise, 'reward_target': env.reward_target, 'random_seed': random_seed,
                      }

        env.save_settings(additional_log=hyp_params)

        model = TD3(MlpPolicy, env, action_noise=action_noise, verbose=1, learning_starts=learning_starts,
                    seed=random_seed, n_cpu_tf_sess=1)
        model.learn(total_timesteps=total_timesteps, log_interval=20)

        env.update_all_plots()
        # env.fig.show()
        # plt.pause(1)
        env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

        # env.fig2.show()
        # plt.pause(1)
        env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

        # env.fig3.show()
        # plt.pause(1)
        env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

        # env.fig5.show()
        # plt.pause(1)
        env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

        model.save(logdir + '/model.h5')


        # =================================================
        # Validation
        # =================================================
        del env

        env = environment(None, state_type=state_type,
                          data_type=data_type, state_dof=state_dof, seed=random_seed,
                          encoder_type=encoder_type, action_space_type='sb',
                          new_model=new_model, no_plot=True,
                          log_path=log_path)

        n_actions = env.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.02 * np.ones(n_actions))

        env.action_scaling = action_scaling
        env.plot_freq = plot_freq
        env.save_freq = save_freq
        env.reward_fraction_sigma = reward_fraction_sigma
        env.rand_x0 = rand_x0
        env.reward_target = new_target

        # Load saved model
        print('Load model')
        model = model.load(logdir + '/model.h5', env=env)

        obs = env.reset()

        # Evaluate the agent
        tot_reward_val = []
        tot_len = []
        episode_reward = 0
        count = 1
        success = False
        for _ in range(100):
              action, _ = model.predict(obs)
              obs, reward, done, info = env.step(action)
              env._render_per_interation()
              episode_reward += reward
              count += 1
              if reward > env.reward_target:
                  success = True
                  episode_reward += 10
              if done:
                      print("Reward:", episode_reward, "Success?", success)
                      env.render()
                      tot_reward_val.append(episode_reward)
                      tot_len.append(count)
                      success = False
                      count = 1
                      episode_reward = 0.0
                      obs = env.reset()

        env.update_all_plots()
        # env.fig.show()
        # plt.pause(1)
        env.fig.savefig(logdir + '/actions' + env.now_str_all + '.png')

        # env.fig2.show()
        # plt.pause(1)
        env.fig2.savefig(logdir + '/all_interactions_' + env.now_str_all + '.png')

        # env.fig3.show()
        # plt.pause(1)
        env.fig3.savefig(logdir + '/stats_' + env.now_str_all + '.png')

        # env.fig5.show()
        # plt.pause(1)
        env.fig5.savefig(logdir + '/states_' + env.now_str_all + '.png')

        pickle.dump((tot_reward_val, tot_len, env.reward_target), 
                    open(logdir + '/val_result_tot.p', 'wb'))

        tot_rew_mean_all[i, j] = np.mean(tot_reward_val)
        tot_rew_std_all[i, j] = np.std(tot_reward_val)

        tot_len_mean_all[i, j] = np.mean(tot_len)
        tot_len_std_all[i, j] = np.std(tot_len)

        tot_target_all[i, j] = env.reward_target

pickle.dump((tot_rew_mean_all,
             tot_rew_std_all,
             tot_len_mean_all, 
             tot_len_std_all, 
             tot_target_all), 
            open(base_dir + 'scan_result.p', 'wb'))
