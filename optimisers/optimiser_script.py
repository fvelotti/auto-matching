from awake_env.tl_opt_environment import tlOptEnv as environment
import matplotlib.pyplot as plt
from helpers.prioritised_experience_replay import PrioritizedReplayBuffer
import numpy as np
import pickle
import pandas as pd
import pyjapc
import datetime

#from bayes_opt import BayesianOptimization
#from zoopt import Dimension, Objective, Parameter, Opt

path = '/afs/cern.ch/user/f/fvelotti/work/public/awake_op_scripts/'
folder_data = path + 'btv_meas_oct19/emit_meas_1/'

class ReplayBufferPER(PrioritizedReplayBuffer):
    """
    A simple FIFO experience replay buffer for DDPG agents.
    """

    def __init__(self, obs_dim, act_dim, size):
        super(ReplayBufferPER, self).__init__(size, 1)

    def store(self, obs, act, rew, next_obs, done):
        super(ReplayBufferPER, self).add(obs, act, rew, next_obs, done, 1)

    def sample_batch(self, batch_size=32):
        obs1, acts, rews, obs2, done, gammas, weights, idxs = super(ReplayBufferPER, self).sample(batch_size, .9)
        return dict(obs1=obs1,
                    obs2=obs2,
                    acts=acts,
                    rews=rews,
                    done=done), [weights, idxs]


class EnvironmentWrapper():
    def __init__(self):
        self.done_flag = False

        self.japc = pyjapc.PyJapc('SPS.USER.ALL')
        self.japc.rbacLogin()
        self.btv_name = 'BTV54'

        self.devices = ['RPSKN.TSG4.SNH.430000',
         'RPSKN.TSG4.SNJ.430001',
#         'RPCAH.TSG4.RCIBH.430011',
#         'RPCAH.TSG4.RCIBV.430011',
#         'RPCAH.TSG4.RCIBH.412353',
#         'RPCAH.TSG4.RCIBV.412353',
         'logical.RQID.430031',
         'logical.RQIF.430034',
         'logical.RQID.430037']

        self.env = environment(self.japc, self.btv_name, self.devices)

        self.dfo = len(self.devices)
        self.replay_buffer = {'time': [], 'penalty': [], 'btv_data': []}
        for dev in self.devices:
            self.replay_buffer[dev] = []
        self.now_str_all = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_')
        self.reset()
        self.o2 = None
        self.off_set = np.zeros(self.dfo)
        self.a = np.zeros(self.dfo)
        self.action_list = []
        self.reward_list = []
        self.reward_xy = []
        self.reward_i = []
        self.episode_nummer = 0
        
        self.fig = plt.figure(1, figsize=(4, 8))
        self.fig.show()
        
        self.fig2 = plt.figure(2, figsize=(4, 6))
        self.fig2.show()

    def reset(self):
        # while True:
        #     self.o, self.r, self.d = self.env.reset(), 0, False
        #     if (self.env.reward) > .3:
        #         break
        self.o, self.r, self.d = self.env.reset(), 0, False
        # self.o = np.zeros(self.dfo)
        self.done_flag = False
        # print('reset: ', self.o)

    def zoopt_objective(self, solution):
        x = solution.get_x()
        return self.objective(x)

    def bayesian_objective(self, angle_cry, pos_cry, do_pos_zs):
        return -1 * self.objective([angle_cry, pos_cry, do_pos_zs])

    def objective(self, a):
        print('asked action ', a)
        a = invNormData(a, constr_limits)
        a = [ele for ele in a]
        self.off_set = 0.0
        self.a = np.array(a)
        delta_a = (self.a - self.off_set)

        print('Action COBYLA: ', self.a)
        self.o2, self.r, self.d, self.data_btv = self.env.step(delta_a)
        
        penalty = np.abs(self.r) - 1.0 * (np.sum(self.data_btv['image']))/(1.3e6) 
        self.now = datetime.datetime.now()
        self.now_str = str(datetime.datetime.now()).replace(' ', '_').replace(':', '_')
        self.store()
        self.r *= 1

        self.action_list.append(self.a)
        self.reward_list.append(penalty)
        self.reward_xy.append(np.abs(self.r))
        self.reward_i.append(1.0 * (np.sum(self.data_btv['image']))/(1.3e6))
        self.o = self.o2
#        self.off_set = self.a
        self.update_plots()
        self.update_plots2()
        self.save_data()
        
        #return np.abs(self.r) / np.sqrt(0.250**2 + 0.250**2) + 0.1 * 1.3e6 / (np.sum(self.data_btv['image']))
        return penalty
    
    def save_data(self):
        pickle.dump(self.replay_buffer, open('data_scan_' + self.now_str_all + '.p', 'wb'))
        
    def store(self):
        self.replay_buffer['time'].append(self.now)
        self.replay_buffer['btv_data'].append(self.data_btv)
        self.replay_buffer['penalty'].append(np.abs(self.r))
        for i, dev in enumerate(self.devices):
            self.replay_buffer[dev].append(self.a[i])
        
    def plot(self):
        data = pd.DataFrame(self.action_list)
        # print(data)
        
        for i, col in enumerate(data.columns):
            a = self.fig.add_subplot(len(data.columns) + 1, 1, i + 1)
            a.plot(data[col])
            plt.legend()
            a.set_ylabel(f'X$_{i}$')
        
#        a = self.fig.add_subplot(len(data.columns) + 1, 1, len(data.columns) + 1)
#        a.plot(np.array(self.reward_list), c='lime')
#        a.set_ylim(0, 5)
        a.set_xlabel('Iterations')
#        a.set_ylabel('Penalty')
        plt.tight_layout()

    def update_plots(self):
          self.fig.clf()
          self.plot()
          self.fig.canvas.draw()
          plt.pause(0.01)

    def plot2(self):
        print('In plot2')
        data = self.data_btv

        a = self.fig2.add_subplot(211)
        
        a.plot(data['x'], data['pro_x'], 'r', label='X = %.2f mm'%data['sigma_x'])
        a.plot(data['x'], data['fit_x'], '--r')
        
        a.plot(data['y'], data['pro_y'], 'k', label='Y = %.2f mm'%data['sigma_y'])
        a.plot(data['y'], data['fit_y'], '--k')
        
        plt.legend()
        plt.ylabel(f'Amplitude')
        plt.xlabel('x, y / mm')
        
        a = self.fig2.add_subplot(212)
        a.plot(np.array(self.reward_list), '-o', color='k', label = 'total',alpha=0.25)
        a.plot(self.reward_i, label='Intensity')
        a.plot(self.reward_xy, label='$\sqrt{\sigma_x^2 + \sigma_y^2}$')
        a.set_ylim(-3, 3)
        a.set_xlabel('Iterations')
        a.set_ylabel('Penalty')
        plt.legend()
        plt.tight_layout()
        
   
    def update_plots2(self):
          self.fig2.clf()
          self.plot2()
          self.fig2.canvas.draw()
          plt.pause(0.01)
          
          
     
def normData(x_data, limits):
    x_data_norm = np.zeros(len(x_data))
    x_ext = np.zeros((len(x_data), 2))
    for i in range(len(x_data)):
        if limits[i] > 40:
            x_ext[i, :] = [0, limits[i]]
        else:
            x_ext[i, :] = [-limits[i], limits[i]]

    # Test to have all of them more or less in the same space
    # x_ext[1:, :] *= 10
    print(x_ext)
    for i in range(len(x_data)):
        x_data_norm[i] = (x_data[i] - x_ext[i, 0]) / (x_ext[i, 1] - x_ext[i, 0])
    return x_data_norm


def invNormData(x_data, limits):
    x_data_norm = np.zeros(len(x_data))
    x_ext = np.zeros((len(x_data), 2))
    for i in range(len(x_data)):
        if limits[i] > 40:
            x_ext[i, :] = [0, limits[i]]
        else:
            x_ext[i, :] = [-limits[i], limits[i]]
    # Test to have all of them more or less in the same space
    # x_ext[1:, :] *= 10
    for i in range(len(x_data)):
        x_data_norm[i] = x_data[i] * (x_ext[i, 1] - x_ext[i, 0]) + x_ext[i, 0]
    return x_data_norm


tl_env = EnvironmentWrapper()

    
limits = [220, 220, 10, 10, 10, 10, 35, 35, 35]



tl_env.reset()
# res = opt.fmin_powell(test.objective, np.zeros(test.dfo))
print('Check dim ', tl_env.dfo)
ini_file = pickle.load(open('../awake_env/initial_para_yday.pkl', 'rb'))
ini_dic = dict(zip(ini_file['devices'], zip(ini_file['values'], limits)))

x0 = []
constr_limits = []
for device in tl_env.devices:
    x0.append(ini_dic[device][0])
#        constr.append(ini_dic[device][1])
    constr_limits.append(ini_dic[device][1])
 
x0 = normData(np.array(x0), constr_limits)

def general_constraint(x):
    global constr_limits
    for i, lim in enumerate(constr_limits):
        if lim > 40:
            if x[i] > 220:
                return -1
        else:
            check = (np.abs(x[i:]) > constr_limits[i:])
            if all(check):
                return -1
            else:
                return 1
            
def general_constraint_norm(x):
    check = (x > 1)
    check2 = (x < 0)
    if any(check) or any(check2):
        return -1
    else:
        return 1

#x0 = x0 * 0.98
#    res = opt.fmin_cobyla(tl_env.objective, x0, general_constraint_norm,
#                          rhobeg=0.18, rhoend=1e-5, catol=1e-7, maxfun=30000)


lower = np.zeros(len(x0))
upper = np.ones(len(x0))

rhobeg = 5e-3
print('Start...')


print(x0)
#res = pybobyqa.solve(tl_env.objective, x0, seek_global_minimum=False, 
#               rhobeg=rhobeg, objfun_has_noise=False, rhoend=1e-4,
#               bounds=(lower,upper))
#
#tl_env.save_data()
#print(res)

# Bayesian optimisation - very slow but very robust
# p_bounds = {'angle_cry': (0, 1), 'pos_cry': (0, 1), 'do_pos_zs': (0, 1)}
# optimiser = BayesianOptimization(f=test.bayesian_objective, pbounds=p_bounds, verbose=2, random_state=1)
# # init-point = random exploration
# # n_iter = interation of gaussian process
# optimiser.maximize(init_points=100, n_iter=100)
# print(optimiser.max)

#
# ZOOPT optimiser => global optimisation - very fast
# obj = Objective(test.zoopt_objective, Dimension(3, [[0, 1], [0, 1], [0, 1]], [True] * 3))
# solution = Opt.min(obj, Parameter(budget=1000 * 3))
# solution.print_solution()

#print('Reward: ', tl_env.r)
# print('Output: ', res)

#tl_env.plot()
#
#filename = 'Scan_data.obj'
#tl_env.store(filename)
#
#filehandler = open(filename, 'rb')
#object = pickle.load(filehandler)


