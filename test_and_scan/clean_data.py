import helpers.anomaly_checks as hp
import os

# %% ---------------------------------------
path = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019/'
files = os.listdir(path)

path_out = '/home/fvelotti/pCloudDrive/Awake/Data/Scans_Nov_2019_cleaned/'
# %% ---------------------------------------

for file_name in files:
    if 'btv' in file_name:

        data = hp.filter_faults(path, file_name, plot=True, write=True, path_out=path_out,
                                plot_location=path_out + f'{file_name[:-2]}.png')


